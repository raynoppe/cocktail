import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { CrudPageRoutingModule } from './crud-page-routing.module';
import { CrudPageComponent } from './crud-page.component';
import { PageHeaderModule } from '../../shared';
import { FormsModule, ReactiveFormsModule} from '@angular/forms';
import { ImageCropperModule, ImageCropperComponent } from 'ngx-img-cropper';

@NgModule({
    imports: [
        CommonModule,
        CrudPageRoutingModule,
        NgbModule.forRoot(),
        PageHeaderModule,
        FormsModule,
        ReactiveFormsModule,
    ],
    declarations: [
        CrudPageComponent
    ]
})
export class CrudPageModule {}
