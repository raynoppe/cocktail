import { Component, OnInit, ViewChild } from '@angular/core';
import { Router, NavigationEnd } from '@angular/router';
import { TranslateService } from '@ngx-translate/core';
import { NgbModal, ModalDismissReasons } from '@ng-bootstrap/ng-bootstrap';
import { AuthService } from '../../../services/auth.service';
import { DataService } from '../../../services/messenger.service';

@Component({
    selector: 'app-header',
    templateUrl: './header.component.html',
    styleUrls: ['./header.component.scss']
})
export class HeaderComponent implements OnInit {
    @ViewChild('content') private content;
    modalRef: any;
    pushRightClass = 'push-right';
    sitename: any;
    name: any;
    family_name: any;
    closeResult: string;
    errHead: any;
    errMess: any;
    message: any;
    showLoader: any;
    alertConfirm = false;
    alertReturn: any;
    alertParams: any;

    constructor(
        private translate: TranslateService,
        public router: Router,
        private authService: AuthService,
        private data: DataService,
        private modalService: NgbModal
        ) {
            this.showLoader = false;
        this.translate.addLangs(['en', 'fr', 'ur', 'es', 'it', 'fa', 'de', 'zh-CHS']);
        this.translate.setDefaultLang('en');
        const browserLang = this.translate.getBrowserLang();
        this.translate.use(browserLang.match(/en|fr|ur|es|it|fa|de|zh-CHS/) ? browserLang : 'en');

        this.router.events.subscribe(val => {
            if (
                val instanceof NavigationEnd &&
                window.innerWidth <= 992 &&
                this.isToggled()
            ) {
                this.toggleSidebar();
            }
        });
        this.sitename = localStorage.getItem('sitename');
        this.errHead = 'Alert';
        this.errMess = 'Loading...';
        this.alertConfirm = false;
        this.alertReturn = {};
        this.alertParams = { buttontext: 'Delete', callfunction: '', callback: {} };
        // this.name = localStorage.getItem('name');
        // this.family_name = localStorage.getItem('family_name');
        this.getUser();
    }

    ngOnInit() {
        this.data.currentMessage.subscribe((message) => {
        this.message = message;
        if (this.message.call === 'alert') {
            this.errHead = this.message.errHead;
            this.errMess = this.message.errMess;
            if (this.message.params) {
                this.alertParams = this.message.params;
                this.alertConfirm = true;
            } else {
                this.alertConfirm = false;
            }
            this.open(this.content);
        }
        if (this.message.call === 'Loader') {
            this.showLoader = true;
        }
        if (this.message.call === 'LoaderHide') {
            this.showLoader = false;
        }
        });
    }

    openM() {
        this.open(this.content);
    }

    open(content) {
        this.modalService.open(content, { centered: true }).result.then((result) => {
            this.closeResult = `Closed with: ${result}`;
        }, (reason) => {
            this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
        });
    }

    private getDismissReason(reason: any): string {
        if (reason === ModalDismissReasons.ESC) {
            return 'by pressing ESC';
        } else if (reason === ModalDismissReasons.BACKDROP_CLICK) {
            return 'by clicking on a backdrop';
        } else {
            return  `with: ${reason}`;
        }
    }

    async getUser() {
        const actUser = await this.authService.getAuthUserInfo();
        this.name = actUser['name'];
        this.family_name = actUser['family_name'];
    }

    isToggled(): boolean {
        const dom: Element = document.querySelector('body');
        return dom.classList.contains(this.pushRightClass);
    }

    toggleSidebar() {
        const dom: any = document.querySelector('body');
        dom.classList.toggle(this.pushRightClass);
    }

    rltAndLtr() {
        const dom: any = document.querySelector('body');
        dom.classList.toggle('rtl');
    }

    onLoggedout() {
        this.authService.logout();
    }

    changeLang(language: string) {
        this.translate.use(language);
    }
    AlertConfirm() {
        this.data.changeMessage({
            call: this.alertParams.callfunction,
            data: this.alertParams.callback,
        });
    }
}
