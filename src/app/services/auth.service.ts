import { Injectable, OnInit, ViewChild, Output, EventEmitter } from '@angular/core';
// import { NavController, NavParams, LoadingController, ToastController, AlertController, Nav } from 'ionic-angular';
import { NgxSpinnerService } from 'ngx-spinner';
import { Router } from '@angular/router';
import { routerTransition } from '../../app/router.animations';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Subject ,  Observable ,  BehaviorSubject } from 'rxjs';

import { User } from '../models/user.model';

import { DataService } from '../services/messenger.service';

import { CognitoUserPool, CognitoUserAttribute, CognitoUser, AuthenticationDetails, CognitoUserSession } from 'amazon-cognito-identity-js';
// console.log('LoginPage',LoginPage);


// const POOL_DATA = {
//   UserPoolId: 'eu-west-2_fxuFKM0Ix',
//   ClientId: '3515iku8559hb04ff5ejm8f84s'
// };
const POOL_DATA = {
  UserPoolId: 'eu-west-1_3klyxmyBi',
  ClientId: 'vb41vespuu148bl1e9vfdooak'
};

const userPool = new CognitoUserPool(POOL_DATA);
// const toastCtrl = ToastController;
const api = '';


@Injectable()
export class AuthService implements OnInit {
  activeUser: any;
  // @ViewChild(Nav) nav: Nav;

  // public navCtrl: NavController;
  // public navParams: NavParams;
  // public toast: ToastController;

  message: string;

  constructor(
    private http: HttpClient,
    private data: DataService,
    private spinner: NgxSpinnerService,
    private router: Router
  ) {
    this.activeUser = {
      jwt: ''
    };
  }

  authIsLoading = new BehaviorSubject<boolean>(false);
  authDidFail = new BehaviorSubject<boolean>(false);
  authPinSent = new BehaviorSubject<boolean>(false);
  authPassReset = new BehaviorSubject<boolean>(false);
  authStatusChanged = new Subject<boolean>();
  registeredUser: CognitoUser;

  ngOnInit () {
    this.data.currentMessage.subscribe(message => this.message = message);
  }

  newMessage(mess) {
    this.data.changeMessage(mess);
  }

  signUp(email: string, password: string, firstname: string, lastname: string): void {
    this.spinner.show();
    // this.spinner.hide();
    this.authIsLoading.next(true);
    const that = this;
    const user: User = {
      // username: username,
      email: email,
      password: password,
      name: firstname,
      family_name: lastname,
      UserType: 'User'
    };
    const attrList: CognitoUserAttribute[] = [];

    const emailAttribute = { Name: 'email', Value: user.email };
    attrList.push(new CognitoUserAttribute(emailAttribute));

    const nameAttribute = { Name: 'name', Value: user.name };
    attrList.push(new CognitoUserAttribute(nameAttribute));
    const family_nameAttribute = { Name: 'family_name', Value: user.family_name };
    attrList.push(new CognitoUserAttribute(family_nameAttribute));
    const usertypeAttribute = { Name: 'custom:UserType', Value: user.UserType };
    attrList.push(new CognitoUserAttribute(usertypeAttribute));

    userPool.signUp(user.email, user.password, attrList, null, (err, result) => {
      this.spinner.hide();
      if (err) {
        const code = err['code'];
        const message = err['message'];
        const name = err['name'];
        window.alert(message);
        this.authDidFail.next(true);
        this.authIsLoading.next(false);
        return;
      }
      this.authDidFail.next(false);
      this.authIsLoading.next(false);
      this.registeredUser = result.user;
      window.alert('Thank you. Please check your e-mail for your activation code');
      this.newMessage({ call: 'registertrue' });
    });
    return;
  }

  confirmUser(username: string, code: string) {
    this.spinner.show();
    const that = this;
    this.authIsLoading.next(true);
    const userData = {
      Username: username,
      Pool: userPool
    };
    const cognitoUser = new CognitoUser(userData);
    cognitoUser.confirmRegistration(code, true, (err, result) => {
      this.spinner.hide();
      if (err) {
        const message = err['message'];
        const name = err['name'];
        window.alert(message);
        this.authDidFail.next(true);
        this.authIsLoading.next(false);
        return;
      }
      this.authDidFail.next(false);
      this.authIsLoading.next(false);
      // this.router.navigate(['/']);
      // that.navCtrl.popToRoot();
      this.newMessage({ call: 'validationtrue' });
      // this.navCtrl.push(LoginPage);
    });
  }

  goToHome() {
    // this.navCtrl.push(LoginPage, {});
  }

  signIn(username: string, password: string): void {
    this.authIsLoading.next(true);
    const authData = {
      Username: username,
      Password: password
    };
    const authDetails = new AuthenticationDetails(authData);
    const userData = {
      Username: username,
      Pool: userPool
    };
    const cognitoUser = new CognitoUser(userData);
    const that = this;
    cognitoUser.authenticateUser(authDetails, {
      onSuccess (result: CognitoUserSession) {
        that.authStatusChanged.next(true);
        that.authDidFail.next(false);
        that.authIsLoading.next(false);
        const idToken = result['idToken'];
        localStorage.setItem('jwtToken', idToken.jwtToken);
        localStorage.setItem('name', idToken.payload.name);
        localStorage.setItem('family_name', idToken.payload.family_name);
        localStorage.setItem('email', idToken.payload.email);
        localStorage.setItem('sub', idToken.payload.sub);
        that.router.navigate(['/']);
      },
      onFailure(err) {
        that.authDidFail.next(true);
        that.authIsLoading.next(false);
        let sendmess = err.message;
        if (sendmess === 'Missing required parameter USERNAME') { sendmess = 'Please enter your registered e-mail'; }
        window.alert(sendmess);
      }
    });
    this.authStatusChanged.next(true);
    return;
  }

  // showAlert(message) {
  //   const alert = this.alertCtrl.create({
  //     title: 'Alert',
  //     subTitle: message,
  //     buttons: ['OK']
  //   });
  //   alert.present();
  // }

  getAuthenticatedUser() {
    return userPool.getCurrentUser();
  }

  async getAuthUserDetailsIn() {
    const cognitoUser = this.getAuthenticatedUser();
    await cognitoUser.getSession((err, session) => {
      if (err) { alert(err.message || JSON.stringify(err)); return; }
      this.activeUser.jwt = session.getIdToken().getJwtToken();
    });
    await cognitoUser.getUserAttributes((err, attributes) => {
      if (err) { alert(err.message || JSON.stringify(err)); return; }
      for (let i = 0; i < attributes.length; i++) {
        this.activeUser[attributes[i].getName()] = attributes[i].getValue();
      }
    });
  }

  async getAuthUserDetails3() {
    return this.activeUser;
  }

  getAuthUserInfo() {
    // return userInfo;
    return new Promise((resolve, reject) => {
      const cognitoUser = userPool.getCurrentUser();
      const userInfo = { jwt: ''};
      cognitoUser.getSession((err, session) => {
        if (err) { alert(err.message || JSON.stringify(err)); return; }
        userInfo.jwt = session.getIdToken().getJwtToken();
        cognitoUser.getUserAttributes((err2, attributes) => {
          if (err) { reject(err); }
          for (let i = 0; i < attributes.length; i++) {
            userInfo[attributes[i].getName()] = attributes[i].getValue();
          }
          resolve(userInfo);
        });
      });
    });
  }

  async getAuthUserDetails() {
    const cognitoUser = this.getAuthenticatedUser();
    const activeUser = { jwt: '', api, sub: '', email: '' };
    await cognitoUser.getSession((err, session) => {
      if (err) { alert(err.message || JSON.stringify(err)); return; }
      activeUser.jwt = session.getIdToken().getJwtToken();
    });
    // cognitoUser. get UserId //
    const cognitoUserId = userPool.getCurrentUser();
    activeUser.sub = cognitoUserId.getUsername();
    // console.log('cognitoUserName');
    // console.log(cognitoUserName);
    // cognitoUserName.getUserAttributes();
    // await cognitoUser.getUserAttributes((err, attributes) => {
    //   if (err) { alert(err.message || JSON.stringify(err)); return; }
    //   for (let i = 0; i < attributes.length; i++) {
    //     const inval = attributes[i].getValue();
    //     activeUser[attributes[i].getName()] = attributes[i].getValue();;
    //   }
    // });
    // this.getAuthUserInformation(cognitoUser);
    return activeUser;
  }

  updateUserAttributes (data) {
    return new Promise((resolve, reject) => {
      const cognitoUser = userPool.getCurrentUser();
      const useData = Object.entries(data);
      const attrList = [];
      for (const [Name, Value] of useData) {
        const val = Value + '';
        if (val !== '') {
          const newrow = { Name, Value: val };
          const attribute = new CognitoUserAttribute(newrow);
          attrList.push(attribute);
        }
      }

      cognitoUser.getSession((err, session) => {
        if (err) { alert(err.message || JSON.stringify(err)); return; }
        cognitoUser.updateAttributes(attrList, function(err2, result) {
          if (err) { reject(err); }
          resolve(result);
        });
      });
    });
  }

  logout() {
    this.getAuthenticatedUser().signOut();
    this.authStatusChanged.next(false);
    localStorage.removeItem('jwtToken');
    localStorage.removeItem('name');
    localStorage.removeItem('family_name');
    localStorage.removeItem('email');
    localStorage.removeItem('sub');
    this.router.navigate(['/login']);
  }

  isAuthenticated(): Observable<boolean> {
    const user = this.getAuthenticatedUser();
    const that = this;
    const obs = Observable.create((observer) => {
      if (!user) {
        observer.next(false);
        this.router.navigate(['/login']);
      } else {
        user.getSession((err, session) => {
          if (err) {
            observer.next(false);
          } else {
            if (session.isValid()) {
              observer.next(true);
            } else {
              observer.next(false);
            }
          }
        });
      }
      observer.complete();
    });
    return obs;
  }

  initAuth() {
    this.isAuthenticated().subscribe(
      (auth) => this.authStatusChanged.next(auth)
    );
  }

  recoverPassword (username: string) {
    this.authIsLoading.next(true);
    const userData = {
      Username: username,
      Pool: userPool
    };
    const cognitoUser = new CognitoUser(userData);
    const that = this;

    cognitoUser.forgotPassword({
      onSuccess: function (data) {
        that.authPinSent.next(true);
        that.authIsLoading.next(false);
          // successfully initiated reset password request
          window.alert('Pin has been sent to your email');
      },
      onFailure: function(err) {
        that.authIsLoading.next(false);
        // window.alert(err.message || JSON.stringify(err));
      },
      // Optional automatic callback
      // inputVerificationCode: function(data) {
      //     console.log('Code sent to: ' + data);
      //     var verificationCode = prompt('Please input verification code ' ,'');
      //     var newPassword = prompt('Enter new password ' ,'');
      //     cognitoUser.confirmPassword(verificationCode, newPassword, {
      //         onSuccess() {
      //             console.log('Password confirmed!');
      //         },
      //         onFailure(err) {
      //             console.log('Password not confirmed!');
      //         }
      //     });
      // }
    });
  }

  verifyPin (username, verificationCode, newPassword) {
    this.authIsLoading.next(true);
    const userData = {
      Username: username,
      Pool: userPool
    };
    const cognitoUser = new CognitoUser(userData);
    const that = this;
    cognitoUser.confirmPassword(verificationCode, newPassword, {
      onFailure(err) {
        that.authIsLoading.next(false);
        const code = err['code'];
        const message = err['message'];
        const name = err['name'];
        window.alert(message);
      },
      onSuccess() {
        that.authIsLoading.next(false);
        that.authPassReset.next(true);
        window.alert('Your password has been changed');
        // that.newMessage({ call: 'pintrue' });
      },
    });
  }

}
