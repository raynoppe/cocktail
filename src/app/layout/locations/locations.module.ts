import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { LocationsRoutingModule } from './locations-routing.module';
import { LocationsComponent } from './locations.component';
import { PageHeaderModule } from '../../shared';
import { FormsModule, ReactiveFormsModule} from '@angular/forms';

@NgModule({
    imports: [
        CommonModule,
        LocationsRoutingModule,
        NgbModule.forRoot(),
        PageHeaderModule,
        FormsModule,
        ReactiveFormsModule,
    ],
    declarations: [
        LocationsComponent
    ]
})
export class LocationsModule {}
