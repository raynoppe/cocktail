export interface User {
  email: string;
  password: string;
  name: string;
  family_name: string;
  UserType: string;
}
