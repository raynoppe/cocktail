import { Component, OnInit } from '@angular/core';
import { routerTransition } from '../../router.animations';
import { FormBuilder, FormArray, FormGroup, FormControl } from '@angular/forms';
import { NgbActiveModal, NgbModal, NgbDate, NgbCalendar } from '@ng-bootstrap/ng-bootstrap';
import { format } from 'date-fns';

import { AuthService } from '../../services/auth.service';
import { locationModel } from '../../models/locations.model';
import { FormService } from '../../services/form.service';
import { DBES } from '../../services/databaseES.service';
import { DataService } from '../../services/messenger.service';
import { LocationsFormComponent } from '../locationsForm/locationsForm.component';
import * as hat from 'hat';
// import { FormModalComponent } from '../../form-modal/form-modal.component';

@Component({
    selector: 'app-locations',
    templateUrl: './locations.component.html',
    styleUrls: ['./locations.component.scss'],
    animations: [routerTransition()]
})
export class LocationsComponent implements OnInit {
    LocationAlias: any;
    message: any;
    SearchForm: FormGroup;
    CrudData: any;
    CrudDataForm: any;
    dti: any;
    isLoading = false;
    saveError = false;
    DataList: any;
    DataCount: any;
    DataListRaw: any;
    DataCountRaw: any;
    SearchType: any;
    QueryString: any;
    ScanString: any;
    ShowMore: boolean;
    ActiveDataRow: any;
    actUser: any;

    // locations I am a part of
    PartOfList: any;
    PartOfListCnt: any;

    // date range
    hoveredDate: NgbDate;
    fromDate: NgbDate;
    toDate: NgbDate;
    fromDateTxt: any;
    toDateTxt: any;
    showDateRangePicker: any;

    // accept invite: start
    AcceptInviteForm: FormGroup;
    AcceptInviteEmail: any;
    AcceptInviteCode: any;
    // accept invite: end

    constructor(
        public formBuilder: FormBuilder,
        public FrmServ: FormService,
        public DB: DBES,
        public authService: AuthService,
        private data: DataService,
        private modalService: NgbModal,
        public calendar: NgbCalendar,
    ) {
            this.LocationAlias = 'Shop';
            // this.dti = FrmServ.convertModel(locationModel(), null); // load the AWS user model

            // this.CrudData = this.dti.dm; // set the form model
            this.actUser = {};
            this.DataList = [];
            this.DataCount = 0;
            this.DataListRaw = [];
            this.DataCountRaw = 0;
            this.ActiveDataRow = -1;
            this.SearchType = 'query';
            this.QueryString = {
                ExpressionAttributeValues: { ':UI': { S: 'user1' } },
                KeyConditionExpression: 'UserId = :UI'
            };
            this.ScanString = {
                ExpressionAttributeValues: { ':UI': { S: 'user1' } },
                FilterExpression: 'UserId = :UI'
            };
            this.ShowMore = false;

            // search form: start
            const searchForm = {};
            searchForm['StringExample'] = new FormControl('');
            this.SearchForm = this.formBuilder.group(searchForm);

            this.fromDateTxt = '';
            this.toDateTxt = '';
            this.showDateRangePicker = false;

            // locations I am a part of
            this.PartOfList = [];
            this.PartOfListCnt = 0;

            // accept invite: start
            const AcceptInviteObj = {};
            AcceptInviteObj['AccInvEmail'] = new FormControl('');
            AcceptInviteObj['AccInvCode'] = new FormControl('');
            this.AcceptInviteForm = this.formBuilder.group(AcceptInviteObj);
            // accept invite: end
        }

    ngOnInit() {
        // this.data.currentMessage.subscribe(message => this.message = message);
        this.data.currentMessage.subscribe((message) => {
            this.message = message;
            if (this.message.call === 'dodelete') {
                this.doDeleteRow(this.message.data);
            }
            if (this.message.call === 'returnrow') {
                if (this.message.rowfunction === 'update') {
                    this.DataList[this.ActiveDataRow]._source = this.message.data;
                } else {
                    this.DataList.push(this.message.data);
                }
            }
        });
        // search form: end
        this.GetExternalData();
        this.LoadMyLocations();
    }

    async GetExternalData() {
        this.actUser = await this.authService.getAuthUserInfo();

        // const AccEmail = this.AcceptInviteForm.value['AccInvEmail'];
        this.AcceptInviteForm.controls['AccInvEmail'].setValue(this.actUser['email']);
    }

    // search and query: start

    async LoadMyLocations() {
        this.data.changeMessage({ call: 'Loader' });
        const actUser = await this.authService.getAuthUserInfo();

        const qobj = {
            FuncType: 'get',
            index: 'users',
            type: 'userList',
            id: actUser['sub']
        };
        const getQ = await this.DB.GetSelf(qobj);
        if ( getQ['_source']['locations'] ) {
            this.PartOfList = getQ['_source']['locations'];
            this.PartOfListCnt = this.PartOfList.length;
        }
        console.log('getQ', this.PartOfList);
        const getLocations = await this.DB.SearchES('locations', 'UserLocations', 'locations.model.js', { query: { match_phrase: { 'Permissions.UserId': actUser['sub'] } } });
        this.DataCountRaw = getLocations['total'];
        this.DataList = getLocations['hits'];
        console.log('DataList', this.DataList);
        this.data.changeMessage({ call: 'LoaderHide' });
    }

    loadMore() {
        this.QueryString['ScanIndexForward'] = true;
        this.LoadMyLocations();
    }

    loadPrevious() {
        this.QueryString['ScanIndexForward'] = false;
        this.LoadMyLocations();
    }
    dataReset() {
        this.SearchType = 'query';
        this.ShowMore = false;
        this.QueryString = {
            ExpressionAttributeValues: { ':UI': { S: 'user1' } },
            KeyConditionExpression: 'UserId = :UI'
        };
        this.LoadMyLocations();
    }

    // search and query: end

    async editRowInModal(row, rowno) {
        this.ActiveDataRow = rowno;
        console.log('row', row);
        const modalRef = this.modalService.open(LocationsFormComponent, { size: 'lg' });
        modalRef.componentInstance.LocationId = row._id;
        modalRef.componentInstance.LocationName = row._source.LocationName;
        modalRef.result.then((result) => {
            console.log(result);
        }).catch((error) => {
            console.log(error);
        });
    }

    async saveForm () {
        if (this.ActiveDataRow < 0) {
            this.data.changeMessage({ call: 'alert', errHead: 'Error', errMess: 'You have not loaded any data from the table above' });
            return;
        }
        // Update the table row
        this.DataList[this.ActiveDataRow] = this.SearchForm.value;
        // Save back to database
        // clean your data
        const newData = JSON.parse(JSON.stringify(this.SearchForm.value));
        // convert the date back into a string in UTC format
        const nd = this.FrmServ.NgDateToUTCString(newData.DateExampleBS);
        newData.DateExampleBS = nd;
        const Result = await this.FrmServ.SaveData('put', 'CrudExample', newData);
    }

    deleteRow(row, rowno) {
        this.data.changeMessage({
            call: 'alert',
            errHead: 'Alert',
            errMess: 'Are you sure you want to delete this row?',
            params: {
                buttontext: 'Delete',
                callfunction: 'dodelete',
                callback: { row, rowno },
            }
        });
    }
    async doDeleteRow(packet) {
        const delData = {
            Key: {
                LocationId: { S: packet.row.LocationId },
                LocationName: { S: packet.row.LocationName },
            }};
            const Result = await this.FrmServ.DeleteData('Locations', delData, false);
        this.DataList.splice(packet.rowno, 1);
    }

    async addNewRecord(row, rowno) {
        this.ActiveDataRow = rowno;
        const modalRef = this.modalService.open(LocationsFormComponent, { size: 'lg' });
        modalRef.componentInstance.LocationId = 'new';
        modalRef.componentInstance.LocationName = '';
        modalRef.result.then((result) => {
            console.log(result);
        }).catch((error) => {
            console.log(error);
        });
    }

    async AcceptInvite() {
        const AccEmail = this.AcceptInviteForm.value['AccInvEmail'];
        const AccCode = this.AcceptInviteForm.value['AccInvCode'];
        if ( AccEmail === '') {
            this.data.changeMessage({ call: 'alert', errHead: 'Error', errMess: 'Please enter a valid email address' });
            return;
        }
        if ( AccCode === '') {
            this.data.changeMessage({ call: 'alert', errHead: 'Error', errMess: 'Please enter the code sent to your email address' });
            return;
        }
        // see if invite exists
        const accept = await this.DB.AcceptInviteEmailES( AccEmail, AccCode );
        console.log('accept return', accept);
        this.data.changeMessage({ call: 'alert', errHead: 'Succes', errMess: 'Thank you. You are now a member.' });
    }
}
