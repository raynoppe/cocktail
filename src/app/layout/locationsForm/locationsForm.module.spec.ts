import { LocationsFormModule } from './locationsForm.module';

describe('locationsFormModule', () => {
    let locationsFormModule: LocationsFormModule;

    beforeEach(() => {
        locationsFormModule = new LocationsFormModule();
    });

    it('should create an instance', () => {
        expect(locationsFormModule).toBeTruthy();
    });
});
