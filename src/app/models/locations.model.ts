export function locationModel() {
  const locationData = [
    { name: 'LocationName', type: 'string', default: '' },
    { name: 'BuildingNumber', type: 'string', default: '' },
    { name: 'Street1', type: 'string', default: '' },
    { name: 'Street2', type: 'string', default: '' },
    { name: 'Town', type: 'string', default: '' },
    { name: 'County', type: 'string', default: '' },
    { name: 'PostCode', type: 'string', default: '' },
    { name: 'Region', type: 'string', default: '' },
    { name: 'Country', type: 'string', default: '' },
    { name: 'LocationImage', type: 'string', default: 'https://fakeimg.pl/200x200/' },
    { name: 'DocStatus', type: 'string', default: '' },
  ];
  return locationData;
}
