import { Injectable, Component, OnInit, Input, HostListener, ViewChild } from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { NgSelectModule, NgOption } from '@ng-select/ng-select';
import { FormBuilder, FormArray, FormGroup, FormControl } from '@angular/forms';
import { ImageCropperComponent, CropperSettings } from 'ngx-img-cropper';
import { DataService } from '../../services/messenger.service';
import { AuthService } from '../../services/auth.service';
import { FormService } from '../../services/form.service';
import { DBES } from '../../services/databaseES.service';
import { locationModel } from '../../models/locations.model';
import * as hat from 'hat';
import { Utils } from '../../services/utils';

@Component({
    selector: 'app-locations-form',
    templateUrl: './locationsForm.component.html',
    styleUrls: ['./locationsForm.component.scss']
})
@Injectable()
export class LocationsFormComponent implements OnInit {
    ModalTitle: any;
    ButtonTitle: any;
    LocationAlias: any;
    message: any;
    @Input() LocationId;
    @Input() LocationName;
    LocationsFormModal: FormGroup;
    LocationData: any;
    dti: any;
    DataListModal: any;
    DataListModalRaw: any;
    newRow: any;
    newRowFirstRun: any;
    actUser: any;
    CountriesList: any;

    // send invite: start
    InviteList: any;
    InviteListCnt: any;
    SendInviteByEmail: FormGroup;
    InviteSubject: any;
    InviteCode: any;
    // send invite: end

    // image cropper: start
    public innerWidth: any;
    imgData: any;
    @ViewChild('cropper', undefined) cropper: ImageCropperComponent;
    cropperSettings: CropperSettings;
    imgPreviewWidth: any;
    imgPreviewHeight: any;
    showCanvas: any;
    // image cropper: end
    constructor(
        private data: DataService,
        public activeModal: NgbActiveModal,
        public FrmServ: FormService,
        public DB: DBES,
        public authService: AuthService,
        public formBuilder: FormBuilder,
        private utils: Utils,
    ) {
        this.LocationAlias = 'Location';
        this.ModalTitle = 'Edit location';
        this.ButtonTitle = 'Save Changes';
        this.actUser = {};
        this.CountriesList = [{ name: 'United Kingdom of Great Britain and Northern Ireland', alpha2Code: 'UK' }];
        this.dti = FrmServ.convertModel(locationModel(), null); // load the AWS user model
        this.LocationData = this.dti.dm; // set the form model
        this.newRow = false;
        this.newRowFirstRun = false;
        // image cropper: start
        this.showCanvas = false;
        this.innerWidth = window.innerWidth; // for image cropper
        let canvaswidth = 217;
        let canvasheight = 217;
        if (this.innerWidth > 991) {
            canvaswidth = 367;
            canvasheight = 367;
        }
        this.cropperSettings = new CropperSettings();
        this.cropperSettings.compressRatio = 0.8;
        this.cropperSettings.width = 100;
        this.cropperSettings.height = 100;
        this.cropperSettings.croppedWidth = 800;
        this.cropperSettings.croppedHeight = 800;
        this.cropperSettings.canvasWidth = canvaswidth;
        this.cropperSettings.canvasHeight = canvasheight;
        this.cropperSettings.noFileInput = true;
        this.imgPreviewWidth = 300;
        this.imgPreviewHeight = 300;
        this.imgData = { image: `https://fakeimg.pl/${this.cropperSettings.croppedWidth}x${this.cropperSettings.croppedHeight}/` };
        // image cropper: end

        // send invite: start
        this.InviteListCnt = 0;
        const SendInviteByEmailForm = {};
        SendInviteByEmailForm['InviteEmail'] = new FormControl('');
        SendInviteByEmailForm['InviteMessage'] = new FormControl('');
        this.SendInviteByEmail = this.formBuilder.group(SendInviteByEmailForm);
        // send invite: end
    }

    ngOnInit() {
        this.data.currentMessage.subscribe((message) => {
            this.message = message;
            if (this.message.call === 'doInviteDelete') {
                this.DeleteInviteDo(this.message.data);
            }
        });
        // this.data.changeMessage({ call: 'alert', errHead: 'Alert', errMess: 'Your content has been loaded' });
        this.LocationsFormModal = this.formBuilder.group(this.dti.dmf);
        if (this.LocationId === 'new') {
            this.setNewRecord();
        } else {
            setTimeout(() => {
                this.loadRecord();
            }, 0);
        }
        this.GetExternalData();
        this.setImageCropperSizes();

    }

    async GetExternalData() {
        this.actUser = await this.authService.getAuthUserInfo();
        this.CountriesList = await this.FrmServ.GetCountries();
    }

    // image cropper: start

    fileChangeListener($event) {
        this.showCanvas = true;
        const image: any = new Image();
        const file: File = $event.target.files[0];
        const myReader: FileReader = new FileReader();
        const that = this;
        myReader.addEventListener('loadend', function (loadEvent: any) {
            image.addEventListener('load', function () {
                that.cropper.setImage(image);
            });
            image.src = loadEvent.target.result;
        });
        myReader.readAsDataURL(file);
    }

    @HostListener('window:resize', ['$event'])
    onResize(event) {
        this.setImageCropperSizes();
    }
    setImageCropperSizes() {
        // 798 397 498 357 576
        if (this.innerWidth < 768) {
            const newsize = document.getElementById('cropcanvas').offsetWidth - 47;
            this.cropperSettings.canvasWidth = newsize;
            this.cropperSettings.canvasHeight = newsize;
        }
    }

    // image cropper: End

    async setNewRecord() {
        this.newRow = true;
        this.newRowFirstRun = true;
        this.ModalTitle = `Add a new ${this.LocationAlias}`;
        this.ButtonTitle = `Add new ${this.LocationAlias}`;
        const newLocationId = hat();
        // this.LocationId = newLocationId;
        // this.LocationsFormModal.controls['LocationId'].setValue(newLocationId);
        this.LocationsFormModal.controls['LocationName'].setValue(this.LocationName);
        // setTimeout(() => {
        //     this.LocationsFormModal.controls['LocationOwner'].setValue(this.actUser['sub']);
        // }, 600);
    }

    async loadRecord() {
        this.data.changeMessage({ call: 'Loader' });
        const QueryString = {
            ExpressionAttributeValues: { ':UI': { S: this.LocationId } },
            KeyConditionExpression: 'LocationId = :UI'
        };
        const getQuery = await this.DB.SearchES('locations', 'UserLocations', 'locations.model.js', { query: { match_phrase: { _id: this.LocationId } } });
        if (getQuery['hits'].length === 0) {
            this.data.changeMessage({ call: 'LoaderHide' });
            this.data.changeMessage({ call: 'alert', errHead: 'Error', errMess: 'No matching record found' });
        } else {
            this.DataListModalRaw = [getQuery['hits'][0]['_source']];
            const FormatRules = [];
            this.DataListModal = await this.FrmServ.MatchModel(this.DataListModalRaw, locationModel(), FormatRules);
            this.data.changeMessage({ call: 'LoaderHide' });
            const row = this.DataListModal[0];
            if (row.LocationImage !== '') { this.imgData.image = row.LocationImage; }
            const rowFormData = this.FrmServ.convertModel(locationModel(), row); // load the AWS user model
            this.LocationData = rowFormData.dm; // set the form model
            this.LocationsFormModal = this.formBuilder.group(rowFormData.dmf);
            // invite message
            console.log('this.actUser', this.actUser);
            const invMessage = `Hi \n${this.actUser.name} ${this.actUser.family_name} has invited you to join ${this.DataListModalRaw[0].LocationName}`;
            this.SendInviteByEmail.controls['InviteMessage'].setValue(invMessage);
            this.InviteSubject = `New invite to join ${this.DataListModalRaw[0].LocationName}`;
            this.InviteCode = this.utils.randomBetween(10000, 99999);
            this.LoadInvites();
        }
    }

    async LoadInvites() {
        this.data.changeMessage({ call: 'Loader' });
        const getQuery = await this.DB.SearchES('alert', 'invite', 'none', { query: { match_phrase: { LocationId: this.LocationId } } });
        console.log('getQuery', getQuery);
        this.InviteListCnt = getQuery['total'];
        this.InviteList = getQuery['hits'];
    }

    createItem(insertRow): FormGroup {
        return this.formBuilder.group(insertRow);
    }

    DeleteInvite(row, rowno) {
        console.log('DeleteInvite', row);
        this.data.changeMessage({
            call: 'alert',
            errHead: 'Alert',
            errMess: 'Are you sure you want to delete this invite?',
            params: {
                buttontext: 'Delete',
                callfunction: 'doInviteDelete',
                callback: { row, rowno },
            }
        });
    }

    async DeleteInviteDo (data) {
        const rowno = data.rowno;
        const row = data.row;
        // const items = this.LocationsFormModal.get('LocationUsers') as FormArray;
        console.log('row._id', row);
        const doDel = await this.DB.DeleteES('alert', 'invite', row._id);
        if( doDel['deleted'] ) {
            const removed = this.InviteList.splice(rowno,1);
        }
    }

    async saveForm () {
        if (this.LocationId === 'new') {
            this.createRecord();
        } else {
            // Save back to database
            this.LocationsFormModal.controls['LocationImage'].setValue(this.imgData.image);
            const Result = await this.DB.UpdateES('locations', 'UserLocations', this.LocationId, 'locations.model.js', this.LocationsFormModal.value);
            const rowfunction = 'update';
            this.data.changeMessage({ call: 'returnrow', data: this.LocationsFormModal.value, rowfunction });
            this.newRow = false;
            this.closeModal();
        }
    }

    async createRecord () {
        // Save back to database
        this.LocationsFormModal.controls['LocationImage'].setValue(this.imgData.image);
        const Result = await this.DB.CreateES('locations', 'UserLocations', 'locations.model.js', this.LocationsFormModal.value);
        const sendObj = {
            _id: Result['_id'],
            _index: Result['_index'],
            _score: 1,
            _source: this.LocationsFormModal.value,
            _type: Result['_type'],
        };
        const rowfunction = 'insert';
        this.data.changeMessage({ call: 'returnrow', data: sendObj, rowfunction });
        this.newRow = false;
        this.closeModal();
    }

    async sendInvite() {
        const Email = this.SendInviteByEmail.value['InviteEmail'];
        const Body = this.SendInviteByEmail.value['InviteMessage'];
        const LocationName = this.LocationsFormModal.value['LocationName'];
        const sendinv = await this.DB.SendInviteES(this.LocationId, LocationName, Email, ' ', ' ', this.InviteSubject, Body, this.InviteCode);
        this.data.changeMessage({ call: 'alert', errHead: 'Success', errMess: 'Invite has been sent' });
        this.SendInviteByEmail.controls['InviteEmail'].setValue('');
        this.LoadInvites();
    }

    closeModal() {
        this.activeModal.close('Modal Closed');
    }
}
