import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { CrudTemplateRoutingModule } from './crudtemplate-routing.module';
import { CrudTemplateComponent } from './crudtemplate.component';
import { PageHeaderModule } from '../../shared';
import { FormsModule, ReactiveFormsModule} from '@angular/forms';

@NgModule({
    imports: [
        CommonModule,
        CrudTemplateRoutingModule,
        NgbModule.forRoot(),
        PageHeaderModule,
        FormsModule,
        ReactiveFormsModule,
    ],
    declarations: [
        CrudTemplateComponent
    ]
})
export class CrudTemplateModule {}
