import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { LocationsFormComponent } from './locationsForm.component';

const routes: Routes = [
    {
        path: '',
        component: LocationsFormComponent
    }
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class LocationsFormRoutingModule {}
