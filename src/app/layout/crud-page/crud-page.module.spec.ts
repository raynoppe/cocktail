import { CrudPageModule } from './crud-page.module';

describe('CrudPageModule', () => {
    let crudPageModule: CrudPageModule;

    beforeEach(() => {
        crudPageModule = new CrudPageModule();
    });

    it('should create an instance', () => {
        expect(crudPageModule).toBeTruthy();
    });
});
