export function userModel() {
  const userData = [
    { name: 'address', type: 'string', default: '' },
    { name: 'birthdate', type: 'string', default: '' },
    { name: 'email', type: 'string', default: '' },
    { name: 'family_name', type: 'string', default: '' },
    { name: 'gender', type: 'string', default: '' },
    { name: 'given_name', type: 'string', default: '' },
    { name: 'locale', type: 'string', default: '' },
    { name: 'middle_name', type: 'string', default: '' },
    { name: 'name', type: 'string', default: '' },
    { name: 'nickname', type: 'string', default: '' },
    { name: 'phone_number', type: 'string', default: '' },
    { name: 'profile', type: 'string', default: '' },
    { name: 'zoneinfo', type: 'string', default: '' },
    { name: 'updated_at', type: 'string', default: '' },
    { name: 'website', type: 'string', default: '' },
  ];
  return userData;
}
