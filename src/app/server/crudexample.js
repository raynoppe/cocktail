var aws = require('aws-sdk');
const dynamodb = new aws.DynamoDB({apiVersion: '2012-08-10', region: 'eu-west-1' });

var ses = new aws.SES({region: 'eu-west-1' });

exports.handler = (event, context, callback) => {
    
    // create a new record
    if (event.FuncType === 'post') {
        const indata = JSON.parse(event.Data); // convert data
        console.log('indata', indata);
        if(indata.UserId === 'random') {
            indata.UserId = "user_" + Math.random();
        }
        if(indata.CrudId === 'random') {
            indata.CrudId = "crud_" + Math.random();
        }
        var marshalled = aws.DynamoDB.Converter.marshall(indata);
        const params = {
            Item: marshalled,
            TableName: event.TableName
        }
        dynamodb.putItem(params, function(err, data) {
            if (err) { 
                const cb = { response: "fail", "data": err.errorType }
                callback(null, cb);
            } else {
                const cb = { response: "success", data: data }
                callback(null, cb);
            }
        });
        
    }
    
    // update a record
    if (event.FuncType === 'put') {
        const indata = JSON.parse(event.Data); // convert data
        var marshalled = aws.DynamoDB.Converter.marshall(indata);
        const params = {
            Item: marshalled,
            TableName: event.TableName
        }
        dynamodb.putItem(params, function(err, data) {
            if (err) { 
                const cb = { response: "fail", "data": err.errorType }
                callback(err, cb);
            } else {
                const cb = { response: "success", data: data }
                callback(null, cb);
            }
        });
    }
    
    // patch a record
    if (event.FuncType === 'patch') {
        const indata = JSON.parse(event.Data); // convert data
        const inParams = JSON.parse(event.Params);
        const ppk = indata[inParams.PPK];
        const psk = indata[inParams.PSK];
        let KCE = `#${inParams.PPK} = :${inParams.PPK} and #${inParams.PSK} = :${inParams.PSK}`;
        const ExprAN = {};
        const ExprAV = {};
        ExprAN["#"+inParams.PPK] = inParams.PPK;
        ExprAN["#"+inParams.PSK] = inParams.PSK;
        ExprAV[":"+inParams.PPK] = { S: ppk };
        ExprAV[":"+inParams.PSK] = { S: psk };
        var params = {
            TableName: event.TableName,
            KeyConditionExpression: KCE,
            ExpressionAttributeNames: ExprAN,
            ExpressionAttributeValues: ExprAV
            };
        dynamodb.query(params, function(err, result) {
            if(err) {
                callback(err);
            } else {
                // remove the primary keys
                delete indata[inParams.PPK];
                delete indata[inParams.PSK];
                var dbData = aws.DynamoDB.Converter.unmarshall(result.Items[0]);
                const useData = Object.entries(indata);
                for (const [Name, Val] of useData) {
                    dbData[Name] = Val;
                }
                
                var marshalled = aws.DynamoDB.Converter.marshall(dbData);
                const params = {
                    Item: marshalled,
                    TableName: event.TableName
                }
                dynamodb.putItem(params, function(err, data) {
                    if (err) { 
                        const cb = { response: "fail", "data": err.errorType }
                        callback(err, cb);
                    } else {
                        const cb = { response: "success", data: data }
                        callback(null, cb);
                    }
                });
            }
        });
    }
    
    
    // query records
    if (event.FuncType === 'query') {
        const Query = JSON.parse(event.Data); // convert Query
        Query['TableName'] = event.TableName;
        var items = [];
        var queryExecute = function(callback) {
            dynamodb.query(Query,function(err,result) {
                if(err) {
                    callback(err);
                } else {
                    const initems = result.Items;
                    const newitems = [];
                    for (let row of initems) {
                        const newrow = aws.DynamoDB.Converter.unmarshall(row);
                        newitems.push(newrow);
                    }
                    result.Items = newitems;
                    callback(null, result)
                }
            });
        }
        queryExecute(callback);
    }
    
    // scan records
    if (event.FuncType === 'scan') {
        const Query = JSON.parse(event.Data); // convert Query
        const inParams = JSON.parse(event.Params);
        // "[{\"operator\": \"none\",\"field\":\"DocumentType\",\"expression\":\"=\",\"value\":\"CV\"}]"
        let FilterExp = "";
        const ExprAN = {};
        const ExprAV = {};
        if(Query != 'none'){
            for (let i = 0; i < Query.length; i++) {
                let fieldname = "#"+Query[i].field;
                let fieldname2 = ":"+Query[i].field;
                let exp = Query[i].expression;
                let operator = Query[i].operator;
                if(operator == 'none'){
                    FilterExp = fieldname + " " + exp + " " +fieldname2;
                } else {
                    FilterExp = FilterExp + " " + operator + " " + fieldname + " " + exp + " " +fieldname2;
                }
                // FilterExp = FilterExp + " and " + fieldname + " " + exp + " " +fieldname2;
                ExprAN[fieldname] = Query[i].field;
                ExprAV[fieldname2] = { S: Query[i].value };
            }
        }
        
        var params = {
            TableName: event.TableName,
            // ProjectionExpression: "DocumentName, FileName",
            FilterExpression: FilterExp,
            ExpressionAttributeNames: ExprAN,
            ExpressionAttributeValues: ExprAV
        };
        // set return fields
        if(inParams.ReturnColumns != '*') {
            params.ProjectionExpression = inParams.ReturnColumns;
        }
        
        console.log('params', params);
        
        var scanExecute = function(callback) {
         
            dynamodb.scan(params,function(err,result) {
                if(err) {
                    callback(err);
                } else {
                     const initems = result.Items;
                    const newitems = [];
                    for (let row of initems) {
                        const newrow = aws.DynamoDB.Converter.unmarshall(row);
                        newitems.push(newrow);
                    }
                    result.Items = newitems;
                    callback(null, result)
                }
            });
        }
         
        scanExecute(callback);
    }
    
    
    
    // console.log(event);
    
    
        // dynamodb.putItem(params, function(err, data) {
        //     if (err) { 
        //         const cb = { response: "fail", "data": err.errorType }
        //         callback(null, cb);
        //     } else {
        //         const cb = { response: "success", data: data }
        //         callback(null, event);
        //     }
        // });

    // if (event.request.userAttributes.email) {
        // sendEmail(event.request.userAttributes.email, "Congratulations " + event.userName + ", you have been confirmed: ", function(status) {

        //     // Return to Amazon Cognito
        //     callback(null, event);
        // });
        // callback(null, event);
    // } else {
        // Nothing to do, the user's email ID is unknown
        // callback(null, event);
    // }
};

function sendEmail(to, body, completedCallback) {
    var eParams = {
        Destination: {
            ToAddresses: [to]
        },
        Message: {
            Body: {
                Text: {
                    Data: body
                }
            },
            Subject: {
                Data: "Cognito Identity Provider registration completed"
            }
        },

        // Replace source_email with your SES validated email address
        Source: "<source_email>"
    };

    var email = ses.sendEmail(eParams, function(err, data){
        if (err) {
            console.log(err);
        } else {
            console.log("===EMAIL SENT===");
        }
        completedCallback('Email sent');
    });
    console.log("EMAIL CODE END");
};