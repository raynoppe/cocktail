export function crudModel() {
    const crudData = [
      { name: 'UserId', type: 'string', default: '' },
      { name: 'CrudId', type: 'string', default: '' },
      { name: 'StringExample', type: 'string', default: '' },
      { name: 'NumberExample', type: 'number', default: 0 },
      { name: 'ObjectExample', type: 'object', default: { Field1: '', Field2: '' } },
      { name: 'ArrayExample', type: 'array', default: [], example: [ { RowField1: 'Row1Value1', RowField2: 'Row1Value2' } ] },
      { name: 'SelectExample', type: 'string', default: 'male' },
      { name: 'CheckBoxExample', type: 'boolean', default: false },
      { name: 'CheckBoxExample2', type: 'boolean', default: false },
      { name: 'RadioButton', type: 'string', default: 'red' },
      { name: 'DateExample', type: 'string', default: '2012-07-27' },
      { name: 'DateExampleBS', type: 'date', default: '2012-07-27' },
      { name: 'TextAreaExample', type: 'string', default: '' },
      { name: 'TextAreaExampleLong', type: 'string', default: '' },
      { name: 'ImageAsBase64', type: 'string', default: '' },
      { name: 'ImageExample', type: 'string', default: 'https://fakeimg.pl/200x200/' }
    ];
    return crudData;
  }
