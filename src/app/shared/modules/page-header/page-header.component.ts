import { Component, OnInit, Input } from '@angular/core';
import { RouterModule } from '@angular/router';

@Component({
    selector: 'app-page-header',
    templateUrl: './page-header.component.html',
    styleUrls: ['./page-header.component.scss']
})
export class PageHeaderComponent implements OnInit {
    @Input() HeaderStyle: string;
    @Input() location: string;
    @Input() locationurl: string;
    @Input() heading: string;
    @Input() icon: string;
    constructor() {
    }

    ngOnInit() {
        if (!this.HeaderStyle) {
            this.HeaderStyle = 'page-header-outer'; // page-header-outer-no-border
        }
    }
}
