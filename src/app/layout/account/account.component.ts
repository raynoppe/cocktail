import { Component, OnInit, ViewChild } from '@angular/core';
import { routerTransition } from '../../router.animations';
import { FormBuilder, FormArray, FormGroup } from '@angular/forms';
import { AuthService } from '../../services/auth.service';
import { userModel } from '../../models/userAWS.model';
import { FormService } from '../../services/form.service';

import { NgbDateParserFormatter, NgbDateStruct } from '@ng-bootstrap/ng-bootstrap';

@Component({
    selector: 'app-account',
    templateUrl: './account.component.html',
    styleUrls: ['./account.component.scss'],
    animations: [routerTransition()]
})
export class AccountComponent implements OnInit {
    AccountForm: FormGroup;
    AccountData: any;
    AccountDataForm: any;
    dti: any;
    isLoading = false;
    saveError = false;

    constructor(public formBuilder: FormBuilder, public FrmServ: FormService, public authService: AuthService) {
        this.dti = FrmServ.convertModel(userModel(), null); // load the AWS user model

        this.AccountData = this.dti.dm; // set the form model
    }

    ngOnInit() {
        this.AccountForm = this.formBuilder.group(this.dti.dmf); // set the group
        this.getAwsUser();
    }

    // get the user details from AWS
    async getAwsUser () {
        const indata = await this.authService.getAuthUserInfo();
        const useData = Object.entries(indata);
        for (const [Name, Val] of useData) {
            // process the data and add conditions
            const infield = Name in this.AccountData;
            if (infield) {
                switch (Name) {
                    case 'birthdate':
                    if (Val !== '') {
                        const senddate = this.FrmServ.DateToNgDate(new Date(Val));
                        console.log('senddate', senddate);
                        this.AccountData[Name] = senddate;
                    }
                    break;
                    default:
                        this.AccountData[Name] = Val;
                    break;
                }
            }
        }
        this.AccountForm.patchValue(this.AccountData);
    }

    async updateUserDetails () {
        this.isLoading = true;
        const newUserDate = JSON.parse(JSON.stringify(this.AccountForm.value));
        const nd = this.FrmServ.NgDateToUTCString(newUserDate.birthdate);
        newUserDate.birthdate = nd;
        const result = await this.authService.updateUserAttributes(newUserDate);
        if (result === 'SUCCESS') {
            this.isLoading = false;
            this.saveError = false;
        } else {
            this.isLoading = false;
            this.saveError = true;
        }
    }

    // save the details back to AWS

    controlSubmit() {}
}
