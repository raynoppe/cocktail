import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { CrudTemplateComponent } from './crudtemplate.component';

const routes: Routes = [
    {
        path: '',
        component: CrudTemplateComponent
    }
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class CrudTemplateRoutingModule {}
