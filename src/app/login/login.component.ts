import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { routerTransition } from '../router.animations';
import { FormBuilder, FormArray, FormGroup } from '@angular/forms';
import { AuthService } from '../services/auth.service';
import { DataService } from '../services/messenger.service';

const usesitename = localStorage.getItem('sitename');

@Component({
    selector: 'app-login',
    templateUrl: './login.component.html',
    styleUrls: ['./login.component.scss'],
    animations: [routerTransition()]
})
export class LoginComponent implements OnInit {
    sitename: any;
    message: any;
    showScreen: any;
    userinformation: any;
    userinformationForm: FormGroup;
    didFail = false;
    pinSent = false;
    passSent = false;
    isLoading = false;

    emailError = false;
    passError = false;
    passLenghtError = false;
    passLowerCaseError = false;
    passUpperCaseError = false;
    passNumberError = false;
    passSpecialError = false;
    passMatchFail = false;
    disableSubmit = true;


    constructor(
        private authService: AuthService,
        private data: DataService,
        public router: Router,
        public formBuilder: FormBuilder,
    ) {
        this.sitename = usesitename;
        this.showScreen = 'login';
        this.userinformation = {
            email: '',
            password: '',
            newpassword: '',
            confirmpassword: '',
            recoverypin: ''
        };
        this.passError = true;
        this.passLenghtError = true;
        this.passLowerCaseError = true;
        this.passUpperCaseError = true;
        this.passNumberError = true;
        this.passSpecialError = true;
        this.passMatchFail = false;
        this.emailError = false;

        this.disableSubmit = true;
    }

    ngOnInit() {
        this.authService.authIsLoading.subscribe(
            (isLoading: boolean) => this.isLoading = isLoading
          );
          this.authService.authDidFail.subscribe(
            (didFail: boolean) => this.didFail = didFail
          );
          this.authService.authPinSent.subscribe(
            (pinSent: boolean) => {
                this.pinSent = pinSent;
                if (pinSent) { this.showScreen = 'havepin'; }
            }
          );
          this.authService.authPassReset.subscribe(
            (passSent: boolean) => {
                this.passSent = passSent;
                console.log('passSent', passSent);

                if (passSent) { this.showScreen = 'login'; }
            }
          );
        this.userinformationForm = this.formBuilder.group({
            email: [''],
            password: [''],
            newpassword: [''],
            confirmpassword: [''],
            recoverypin: [''],
          });
    }

    // login
    backToLogin () {
        this.showScreen = 'login';
    }
    onLoggedin() {
        localStorage.setItem('isLoggedin', 'true');
    }
    onSubmit() {
        const usrName = this.userinformation.email;
        const password = this.userinformation.password;
        console.log('usrName', usrName, 'password', password);
        this.authService.signIn(usrName, password);
      }

    // recover pin
    troubleLoginIn() {
        this.showScreen = 'trouble';
    }
    sendRecoveryPin () {
        const email = this.userinformation.email;
        this.authService.recoverPassword(email);
    }

    // enter pin
    havePin() {
        console.log('have pin');
        this.showScreen = 'havepin';
    }

    validateEmail() {
        const email = this.userinformation.email;
        const re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
        if (!re.test(email)) {
          this.emailError = true;
        } else {
          this.emailError = false;
        }
        this.controlSubmit();
      }

      checkPassword() {
          // var re = /^(?=.*\d)(?=.*[!@#$%^&*])(?=.*[a-z])(?=.*[A-Z]).{8,}$/;
          const re = /^(?=.*[!@#$%^&*])$/;
          // return re.test(str);

          this.passError = false;
          this.passLenghtError = false;
          this.passLowerCaseError = false;
          this.passUpperCaseError = false;
          this.passNumberError = false;
          this.passSpecialError = false;

          const p = this.userinformation.newpassword;
          if (p.length < 8) {
            this.passError = true;
            this.passLenghtError = true;
          }
          if (p.search(/[a-z]/) < 0) {
            this.passError = true;
            this.passLowerCaseError = true;
          }
          if (p.search(/[A-Z]/) < 0) {
            this.passError = true;
            this.passUpperCaseError = true;
          }
          if (p.search(/[0-9]/) < 0) {
            this.passError = true;
            this.passNumberError = true;
          }
          if (p.search(/.*[!@#$%^&*() =+_-]/) < 0) {
            this.passError = true;
            this.passSpecialError = true;
          }
          this.passwordMatch();
      }

      passwordMatch() {
        if (this.userinformation.newpassword === this.userinformation.confirmpassword) {
          this.passMatchFail = false;
        } else {
          this.passMatchFail = true;
        }
        this.controlSubmit();
      }

      controlSubmit() {
        let showSubmit = true;
        if (this.userinformation.email === '') { showSubmit = false; }
        if (this.userinformation.recoverypin === '') { showSubmit = false; }
        if (this.passError === true) { showSubmit = false; }
        if (this.passMatchFail === true) { showSubmit = false; }

        if (showSubmit === true) {
          this.disableSubmit = false;
        }
      }

      changePassword() {
        const username = this.userinformation.email;
        const verificationCode = this.userinformation.recoverypin;
        const newPassword = this.userinformation.newpassword;
        this.authService.verifyPin (username, verificationCode, newPassword);
      }
}
