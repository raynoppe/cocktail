import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CrudTemplateComponent } from './crudtemplate.component';

describe('CrudTemplateComponent', () => {
    let component: CrudTemplateComponent;
    let fixture: ComponentFixture<CrudTemplateComponent>;

    beforeEach(
        async(() => {
            TestBed.configureTestingModule({
                declarations: [CrudTemplateComponent]
            }).compileComponents();
        })
    );

    beforeEach(() => {
        fixture = TestBed.createComponent(CrudTemplateComponent);
        component = fixture.componentInstance;
        fixture.detectChanges();
    });

    it('should create', () => {
        expect(component).toBeTruthy();
    });
});
