import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { UsersRoutingModule } from './users-routing.module';
import { UsersComponent } from './users.component';
import { PageHeaderModule } from '../../shared';
import { FormsModule, ReactiveFormsModule} from '@angular/forms';

@NgModule({
    imports: [
        CommonModule,
        UsersRoutingModule,
        NgbModule.forRoot(),
        PageHeaderModule,
        FormsModule,
        ReactiveFormsModule,
    ],
    declarations: [
        UsersComponent
    ]
})
export class UsersModule {}
