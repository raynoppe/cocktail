export function privilegesModel() {
    const privilegesData = [
      { name: 'PrivilegesId', type: 'string', default: '' },
      { name: 'PrivilegeName', type: 'string', default: '' },
      { name: 'LocationId', type: 'string', default: '' },
      { name: 'AreaId', type: 'string', default: '' },
      { name: 'UserId', type: 'string', default: '' },
      { name: 'Read', type: 'boolean', default: false },
      { name: 'Edit', type: 'boolean', default: false },
      { name: 'Approve', type: 'boolean', default: false },
      { name: 'Publish', type: 'boolean', default: false },
      { name: 'Create', type: 'boolean', default: false },
      { name: 'Archive', type: 'boolean', default: false },
      { name: 'Delete', type: 'boolean', default: false },
      { name: 'Send', type: 'boolean', default: false },
    ];
    return privilegesData;
  }
