import { Injectable, Component, OnInit, Input, HostListener, ViewChild } from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { FormBuilder, FormArray, FormGroup } from '@angular/forms';
import { ImageCropperComponent, CropperSettings } from 'ngx-img-cropper';
import { DataService } from '../../services/messenger.service';
import { AuthService } from '../../services/auth.service';
import { FormService } from '../../services/form.service';
import { crudModel } from '../../models/crud.model';

@Component({
    selector: 'app-crud-form',
    templateUrl: './crud-form.component.html',
    styleUrls: ['./crud-form.component.scss']
})
@Injectable()
export class CrudFormComponent implements OnInit {
    message: any;
    @Input() UserId;
    @Input() CrudId;
    CrudFormModal: FormGroup;
    CrudData: any;
    dti: any;
    DataListModal: any;
    DataListModalRaw: any;
    newRow: any;
    // image cropper
    public innerWidth: any;
    imgData: any;
    @ViewChild('cropper', undefined) cropper: ImageCropperComponent;
    cropperSettings: CropperSettings;
    imgPreviewWidth: any;
    imgPreviewHeight: any;
    constructor(
        private data: DataService,
        public activeModal: NgbActiveModal,
        public FrmServ: FormService,
        public authService: AuthService,
        public formBuilder: FormBuilder,
    ) {
        this.dti = FrmServ.convertModel(crudModel(), null); // load the AWS user model
        this.CrudData = this.dti.dm; // set the form model
        console.log('this.dti', this.dti);
        this.newRow = false;
        // image cropper
        this.innerWidth = window.innerWidth; // for image cropper
        let canvaswidth = 217;
        let canvasheight = 217;
        if (this.innerWidth > 991) {
            canvaswidth = 367;
            canvasheight = 367;
        }
        this.cropperSettings = new CropperSettings();
        this.cropperSettings.compressRatio = 0.8;
        this.cropperSettings.width = 100;
        this.cropperSettings.height = 100;
        this.cropperSettings.croppedWidth = 800;
        this.cropperSettings.croppedHeight = 800;
        this.cropperSettings.canvasWidth = canvaswidth;
        this.cropperSettings.canvasHeight = canvasheight;
        this.cropperSettings.noFileInput = true;
        this.imgPreviewWidth = 300;
        this.imgPreviewHeight = 300;
        this.imgData = { image: `https://fakeimg.pl/${this.cropperSettings.croppedWidth}x${this.cropperSettings.croppedHeight}/` };
    }

    ngOnInit() {
        this.data.currentMessage.subscribe(message => this.message = message);
        // this.data.changeMessage({ call: 'alert', errHead: 'Alert', errMess: 'Your content has been loaded' });
        console.log('UserId', this.UserId, 'CrudId', this.CrudId);
        this.CrudFormModal = this.formBuilder.group(this.dti.dmf);
        if (this.UserId === 'new') {
            this.setNewRecord();
        } else {
            setTimeout(() => {
                this.loadRecord();
            }, 0);
        }
        console.log('this.innerWidth', this.innerWidth);
        this.setImageCropperSizes();
    }

    fileChangeListener($event) {
        const image: any = new Image();
        const file: File = $event.target.files[0];
        const myReader: FileReader = new FileReader();
        const that = this;
        myReader.addEventListener('loadend', function (loadEvent: any) {
          image.addEventListener('load', function () {
            that.cropper.setImage(image);
          });
          image.src = loadEvent.target.result;
        });
        myReader.readAsDataURL(file);
      }

    @HostListener('window:resize', ['$event'])
    onResize(event) {
        this.setImageCropperSizes();
    }
    setImageCropperSizes() {
        // 798 397 498 357 576
        console.log('this.innerWidth new', this.innerWidth);
        if (this.innerWidth < 768) {
            console.log('Call me', this.innerWidth);
            const newsize = document.getElementById('cropcanvas').offsetWidth - 47;
            this.cropperSettings.canvasWidth = newsize;
            this.cropperSettings.canvasHeight = newsize;
        }
    }

    async setNewRecord() {
        this.newRow = true;
        const actUser = await this.authService.getAuthUserInfo();
        this.CrudFormModal.controls['UserId'].setValue(actUser['sub']);
        this.CrudFormModal.controls['CrudId'].setValue(this.CrudId);
        console.log('this.CrudFormModal', this.CrudFormModal);
    }

    async loadRecord() {
        this.data.changeMessage({ call: 'Loader' });
        const QueryString = {
            ExpressionAttributeValues: { ':UI': { S: this.UserId }, ':CI': { S: this.CrudId } },
            KeyConditionExpression: 'UserId = :UI and CrudId = :CI'
        };
        const getQuery = await this.FrmServ.dbSearch(
            'query',
            'CrudExample',
            QueryString
        );
        if (getQuery['Count'] === 0) {
            this.data.changeMessage({ call: 'LoaderHide' });
            this.data.changeMessage({ call: 'alert', errHead: 'Error', errMess: 'No matching record found' });
        } else {
            this.DataListModalRaw = getQuery['Items'];
            const FormatRules = [
                { 'field': 'NumberExample', 'format': 'number' },
                { 'field': 'DateExampleBS', 'format': 'date' }
            ];
            this.DataListModal = await this.FrmServ.MatchModel(this.DataListModalRaw, crudModel(), FormatRules);
            this.data.changeMessage({ call: 'LoaderHide' });
            const row = this.DataListModal[0];
            const rowFormData = this.FrmServ.convertModel(crudModel(), row); // load the AWS user model
            this.CrudData = rowFormData.dm; // set the form model
            this.CrudFormModal = this.formBuilder.group(rowFormData.dmf);
        }
    }

    createItem(): FormGroup {
        return this.formBuilder.group({
            RowField1: '',
            RowField2: ''
        });
      }

    AddANewRow() {
        const items = this.CrudFormModal.get('ArrayExample') as FormArray;
        items.push(this.createItem());
    }

    DeleteARow(rowno) {
        const items = this.CrudFormModal.get('ArrayExample') as FormArray;
        items.removeAt(rowno);
    }

    async saveForm () {
        console.log('saveForm');
        // Save back to database
        // clean your data
        const newData = JSON.parse(JSON.stringify(this.CrudFormModal.value));
        // convert the date back into a string in UTC format
        const nd = this.FrmServ.NgDateToUTCString(newData.DateExampleBS);
        newData.DateExampleBS = nd;
        newData.ImageAsBase64 = this.imgData.image;
        this.CrudFormModal.value.ImageAsBase64 = this.imgData.image;
        const Result = await this.FrmServ.SaveData('put', 'CrudExample', newData);
        let rowfunction = 'update';
        if (this.newRow) {
            rowfunction = 'insert';
        }
        this.data.changeMessage({ call: 'returnrow', data: this.CrudFormModal.value, rowfunction });
        this.newRow = false;
        this.closeModal();
    }

    closeModal() {
        this.activeModal.close('Modal Closed');
    }
}
