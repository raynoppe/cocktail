import { CommonModule } from '@angular/common';
import { HttpClient, HttpClientModule } from '@angular/common/http';
import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { TranslateLoader, TranslateModule } from '@ngx-translate/core';
import { TranslateHttpLoader } from '@ngx-translate/http-loader';
import { NgxSpinnerModule } from 'ngx-spinner';
import { ImageCropperComponent } from 'ngx-img-cropper';
import { NgSelectModule } from '@ng-select/ng-select';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { AuthGuard } from './shared';

import { AuthService } from '../app/services/auth.service';
import { DataService } from './services/messenger.service';
import { FormService } from '../app/services/form.service';
import { DBES } from '../app/services/databaseES.service';
import { FormBuilder } from '@angular/forms';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { NgbModule, NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { CrudFormComponent } from '../app/layout/crud-form/crud-form.component';
import { LocationsFormComponent } from './layout/locationsForm/locationsForm.component';
import { Utils } from './services/utils';

// AoT requires an exported function for factories
export const createTranslateLoader = (http: HttpClient) => {
    /* for development
    return new TranslateHttpLoader(
        http,
        '/start-angular/SB-Admin-BS4-Angular-6/master/dist/assets/i18n/',
        '.json'
    ); */
    return new TranslateHttpLoader(http, './assets/i18n/', '.json');
};

@NgModule({
    imports: [
        CommonModule,
        BrowserModule,
        BrowserAnimationsModule,
        HttpClientModule,
        NgxSpinnerModule,
        TranslateModule.forRoot({
            loader: {
                provide: TranslateLoader,
                useFactory: createTranslateLoader,
                deps: [HttpClient]
            }
        }),
        AppRoutingModule,
        NgbModule.forRoot(),
        FormsModule,
        ReactiveFormsModule,
        NgSelectModule,
    ],
    declarations: [AppComponent, CrudFormComponent, ImageCropperComponent, LocationsFormComponent],
    providers: [
        AuthGuard,
        AuthService,
        DataService,
        FormService,
        DBES,
        FormBuilder,
        NgbActiveModal,
        Utils,
    ],
    bootstrap: [AppComponent],
    entryComponents: [CrudFormComponent, LocationsFormComponent]
})
export class AppModule {}
