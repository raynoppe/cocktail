import { Component, OnInit } from '@angular/core';
import { routerTransition } from '../router.animations';
import { FormControl } from '@angular/forms';
import { AuthService } from '../services/auth.service';
import { DataService } from '../services/messenger.service';
import { Router } from '@angular/router';

const usesitename = localStorage.getItem('sitename');

@Component({
    selector: 'app-signup',
    templateUrl: './signup.component.html',
    styleUrls: ['./signup.component.scss'],
    animations: [routerTransition()]
})
export class SignupComponent implements OnInit {
  sitename: any;
    user: any;
    confirmUser = false;
    didFail = false;
    isLoading = false;
    emailError = false;
    passMatchFail = false;
    disableSubmit = true;

    passError = false;
    passLenghtError = false;
    passLowerCaseError = false;
    passUpperCaseError = false;
    passNumberError = false;
    passSpecialError = false;

    firstname = new FormControl('');
    lastname = new FormControl('');
    email = new FormControl('');
    confirmemail = new FormControl('');
    password = new FormControl('');
    passwordconfirm = new FormControl('');
    validationCode = new FormControl('');

    message: any;

    constructor(
        private authService: AuthService,
        private data: DataService,
        public router: Router
    ) {
      this.sitename = usesitename;
        this.passError = true;
        this.passLenghtError = true;
        this.passLowerCaseError = true;
        this.passUpperCaseError = true;
        this.passNumberError = true;
        this.passSpecialError = true;
        this.passMatchFail = false;

        this.disableSubmit = true;
    }

    ngOnInit() {
      this.authService.authIsLoading.subscribe(
        (isLoading: boolean) => this.isLoading = isLoading
      );
      this.authService.authDidFail.subscribe(
        (didFail: boolean) => this.didFail = didFail
      );
      this.data.currentMessage.subscribe((message) => {
          this.message = message;
          if (this.message.call === 'registertrue') {
            this.confirmUser = true;
          }
          if (this.message.call === 'validationtrue') {
          //   this.navCtrl.push(LoginPage, { email: this.user.email });
          this.router.navigate(['/login']);
          }
        });
    }

    validateEmail() {
        const email = this.email.value;
        const re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
        if (!re.test(email)) {
          this.emailError = true;
        } else {
          this.emailError = false;
        }
      }
      checkPassword() {
          // var re = /^(?=.*\d)(?=.*[!@#$%^&*])(?=.*[a-z])(?=.*[A-Z]).{8,}$/;
          const re = /^(?=.*[!@#$%^&*])$/;
          // return re.test(str);
          this.passError = false;
          this.passLenghtError = false;
          this.passLowerCaseError = false;
          this.passUpperCaseError = false;
          this.passNumberError = false;
          this.passSpecialError = false;
          const p = this.password.value;
          if (p.length < 8) {
            this.passError = true;
            this.passLenghtError = true;
          }
          if (p.search(/[a-z]/) < 0) {
            this.passError = true;
            this.passLowerCaseError = true;
          }
          if (p.search(/[A-Z]/) < 0) {
            this.passError = true;
            this.passUpperCaseError = true;
          }
          if (p.search(/[0-9]/) < 0) {
            this.passError = true;
            this.passNumberError = true;
          }
          if (p.search(/.*[!@#$%^&*() =+_-]/) < 0) {
            this.passError = true;
            this.passSpecialError = true;
          }
          this.passwordMatch();
      }

      passwordMatch() {
        if (this.password.value === this.passwordconfirm.value) {
          this.passMatchFail = false;
        } else {
          this.passMatchFail = true;
        }
        this.controlSubmit();
      }

      controlSubmit() {
        let showSubmit = true;
        if (this.firstname.value === '') { showSubmit = false; }
        if (this.lastname.value === '') { showSubmit = false; }
        if (this.passError === true) { showSubmit = false; }
        if (this.passMatchFail === true) { showSubmit = false; }

        if (showSubmit === true) {
          this.disableSubmit = false;
        }
      }

      onSubmit() {
        const email = this.email.value;
        const password = this.password.value;
        const firstname = this.firstname.value;
        const lastname = this.lastname.value;
        // this.authService.signUp(usrName, email, password);
        this.authService.signUp(email, password, firstname, lastname);
      }

      onDoConfirm() {
        this.confirmUser = true;
      }

      onConfirm() {
        this.authService.confirmUser(this.email.value, this.validationCode.value);
      }

      cancel() {
        this.confirmUser = false;
      }

      register() {
        // this.navCtrl.push(HomePage, {});
        // this.menuCtrl.enable(true);
      }
}
