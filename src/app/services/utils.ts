export class Utils {
    isNumber(value) { return typeof value === 'number'; }
    isUndefined(value) { return typeof value === 'undefined'; }
    isDefined(value) { return typeof value !== 'undefined'; }
    isObject(value) {
        // http://jsperf.com/isobject4
        return value !== null && typeof value === 'object';
    }
    isString(value) { return typeof value === 'string'; }
    isDate(value) {
        return toString.call(value) === '[object Date]';
    }
    // isArray = Array.isArray;
    isFunction(value) { return typeof value === 'function'; }
    isRegExp(value) {
        return toString.call(value) === '[object RegExp]';
    }
    padNumber(num, digits, trim) {
        let neg = '';
        if (num < 0) {
            neg =  '-';
            num = -num;
        }
        num = '' + num;
        while (num.length < digits) { num = '0' + num; }
        if (trim) {
            num = num.substr(num.length - digits);
        }
        return neg + num;
    }
    escapeSpecialChars(text) {
        return text.replace(/[\\]/g, '\\\\')
        .replace(/[\"]/g, '\\\"')
        .replace(/[\/]/g, '\\/')
        .replace(/[\b]/g, '\\b')
        .replace(/[\f]/g, '\\f')
        .replace(/[\n]/g, '\\n')
        .replace(/[\r]/g, '\\r')
        .replace(/[\t]/g, '\\t');
    }

    randomBetween(min, max) {
        return Math.floor(Math.random() * ( max - min + 1 ) + min );
    }
}
