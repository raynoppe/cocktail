import { CrudFormModule } from './crud-form.module';

describe('CrudPageModule', () => {
    let crudPageModule: CrudFormModule;

    beforeEach(() => {
        crudPageModule = new CrudFormModule();
    });

    it('should create an instance', () => {
        expect(crudPageModule).toBeTruthy();
    });
});
