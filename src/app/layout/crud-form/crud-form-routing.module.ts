import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { CrudFormComponent } from './crud-form.component';

const routes: Routes = [
    {
        path: '',
        component: CrudFormComponent
    }
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class CrudFormRoutingModule {}
