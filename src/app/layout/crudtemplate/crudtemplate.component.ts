import { Component, OnInit } from '@angular/core';
import { routerTransition } from '../../router.animations';
import { FormBuilder, FormArray, FormGroup, FormControl } from '@angular/forms';
import { NgbActiveModal, NgbModal, NgbDate, NgbCalendar } from '@ng-bootstrap/ng-bootstrap';
import { format, subDays, startOfMonth, endOfMonth } from 'date-fns';

import { AuthService } from '../../services/auth.service';
import { crudModel } from '../../models/crud.model';
import { FormService } from '../../services/form.service';
import { DataService } from '../../services/messenger.service';
import { CrudFormComponent } from '../crud-form/crud-form.component';
import * as hat from 'hat';
// import { FormModalComponent } from '../../form-modal/form-modal.component';

@Component({
    selector: 'app-crudtemplate',
    templateUrl: './crudtemplate.component.html',
    styleUrls: ['./crudtemplate.component.scss'],
    animations: [routerTransition()]
})
export class CrudTemplateComponent implements OnInit {
    message: any;
    SearchForm: FormGroup;
    CrudData: any;
    CrudDataForm: any;
    dti: any;
    isLoading = false;
    saveError = false;
    DataList: any;
    DataCount: any;
    DataListRaw: any;
    DataCountRaw: any;
    SearchType: any;
    QueryString: any;
    ScanString: any;
    ShowMore: boolean;
    ActiveDataRow: any;

    // date range
    hoveredDate: NgbDate;
    fromDate: NgbDate;
    toDate: NgbDate;
    fromDateTxt: any;
    toDateTxt: any;
    showDateRangePicker: any;

    constructor(
        public formBuilder: FormBuilder,
        public FrmServ: FormService,
        public authService: AuthService,
        private data: DataService,
        private modalService: NgbModal,
        public calendar: NgbCalendar
        ) {
        this.dti = FrmServ.convertModel(crudModel(), null); // load the AWS user model

        this.CrudData = this.dti.dm; // set the form model

        this.DataList = [];
        this.DataCount = 0;
        this.DataListRaw = [];
        this.DataCountRaw = 0;
        this.ActiveDataRow = -1;
        this.SearchType = 'query';
        this.QueryString = {
            ExpressionAttributeValues: { ':UI': { S: 'user1' } },
            KeyConditionExpression: 'UserId = :UI'
        };
        this.ScanString = {
            ExpressionAttributeValues: { ':UI': { S: 'user1' } },
            FilterExpression: 'UserId = :UI'
        };
        this.ShowMore = false;

        // search form: start
        const searchForm = {};
        searchForm['StringExample'] = new FormControl('');
        this.SearchForm = this.formBuilder.group(searchForm);

        this.fromDateTxt = '';
        this.toDateTxt = '';
        this.showDateRangePicker = false;
    }

    ngOnInit() {
        // this.data.currentMessage.subscribe(message => this.message = message);
        this.data.currentMessage.subscribe((message) => {
            this.message = message;
            if (this.message.call === 'dodelete') {
                this.doDeleteRow(this.message.data);
            }
            if (this.message.call === 'returnrow') {
                if (this.message.rowfunction === 'update') {
                    this.DataList[this.ActiveDataRow] = this.message.data;
                } else {
                    this.DataList.push(this.message.data);
                }
            }
        });
        console.log('this.SearchForm', this.SearchForm);
        // search form: end

        this.LoadData();
    }

    // search and query: start

    async LoadData() {
        this.data.changeMessage({ call: 'Loader' });
        const actUser = await this.authService.getAuthUserInfo();
        this.QueryString.ExpressionAttributeValues = { ':UI': { S: actUser['sub'] } },
        this.QueryString.KeyConditionExpression = 'UserId = :UI';
        /*
        Call: dbSearch(FuncType, TableName, Params, Conditions)
        In File: /src/app/services/form.services.ts
        Description: Get data from the database useing a query
        Attributes:
        FuncType: query or scan
        TableName: The table name in DynamoDB
        Query:
            ExpressionAttributeValues: { ':UI': { S: 'user1' } } *Required
            KeyConditionExpression: 'UserId = :UI' *Required Operators: = | < | <= | > | >= | BETWEEN | begins_with ( sortKeyName, :sortkeyval )
            IndexName: If you are using an index against the table UserId-DateExampleBS-index
            Limit: 10 *Integer
            ProjectionExpression: 'UserId, StringExample' String with a list of fields you want returned
            ExpressionAttributeNames: {"#U":"UserId"}
            ExclusiveStartKey
            For more information: https://docs.aws.amazon.com/AWSJavaScriptSDK/latest/AWS/DynamoDB.html#query-property
        */
        let SearchString = this.QueryString;
        if (this.SearchType === 'scan') {
            SearchString = this.ScanString;
        }
        const getQuery = await this.FrmServ.dbSearch(
            this.SearchType,
            'CrudExample',
            SearchString
        );
        this.DataListRaw = getQuery['Items'];
        this.DataCountRaw = getQuery['Count'];
        // pagination.
        if (getQuery['LastEvaluatedKey']) {
            this.QueryString['ExclusiveStartKey'] = getQuery['LastEvaluatedKey'];
            this.ShowMore = true;
        }
        const FormatRules = [
            { 'field': 'NumberExample', 'format': 'number' },
            { 'field': 'DateExampleBS', 'format': 'date' },
            { 'field': 'ImageExample', 'format': 'string' },
        ];
        this.DataList = await this.FrmServ.MatchModel(this.DataListRaw, crudModel(), FormatRules);
        this.data.changeMessage({ call: 'LoaderHide' });
    }

    sortSearch() {
        this.QueryString['IndexName'] = 'UserId-DateExampleBS-index';
        this.LoadData();
    }

    limitSearch() {
        console.log('Limit search');
        this.QueryString['Limit'] = 2;
        this.LoadData();
    }

    loadMore() {
        console.log('Load More');
        this.QueryString['ScanIndexForward'] = true;
        this.LoadData();
    }

    loadPrevious() {
        this.QueryString['ScanIndexForward'] = false;
        console.log('Load Previous');
        this.LoadData();
    }
    dataReset() {
        console.log('Reset');
        this.SearchType = 'query';
        this.ShowMore = false;
        this.QueryString = {
            ExpressionAttributeValues: { ':UI': { S: 'user1' } },
            KeyConditionExpression: 'UserId = :UI'
        };
        this.LoadData();
    }

    scanData() {
        // I am including this example BUT I strongly advise against scan especially on big tables.
        this.SearchType = 'scan';
        console.log('scanData');
        let doScan = false;
        const ExpressionAttributeValuesObj = {};
        const FilterExpressionArr = [];
        const searchText = this.SearchForm.value.StringExample;
        if (searchText !== '') {
            ExpressionAttributeValuesObj[':s'] = { S: searchText };
            FilterExpressionArr.push('contains(StringExample, :s)');
            doScan = true;
        }
        if (this.fromDateTxt !== '') {
            const fmDate = this.FrmServ.NgDateToUTCString(this.fromDate);
            const enDate = this.FrmServ.NgDateToUTCString(this.toDate);
            ExpressionAttributeValuesObj[':f'] = { S: fmDate };
            ExpressionAttributeValuesObj[':t'] = { S: enDate };
            FilterExpressionArr.push('DateExampleBS BETWEEN :f AND :t');
            doScan = true;
        }
        const FilterExpressionStr = FilterExpressionArr.toString();
        this.ScanString.ExpressionAttributeValues = ExpressionAttributeValuesObj;
        this.ScanString.FilterExpression = FilterExpressionStr;
        console.log('this.ScanString', this.ScanString);
        if (doScan) {
            this.LoadData();
        } else {
            this.data.changeMessage({ call: 'alert', errHead: 'Error', errMess: 'Please enter a search condition' });
        }
    }

    // search date: start

    showDateRange() {
        this.showDateRangePicker = true;
        if (this.fromDateTxt === '') {
            this.fromDate = this.calendar.getToday();
            this.toDate = this.calendar.getNext(this.calendar.getToday(), 'd', 10);
            this.fromDateTxt = format(this.FrmServ.NgDateToUTCString(this.fromDate), 'ddd DD MMM YYYY');
            this.toDateTxt = format(this.FrmServ.NgDateToUTCString(this.toDate), 'ddd DD MMM YYYY');
        }
    }
    clearDateRange() {
        this.fromDate = null;
        this.toDate = null;
        this.fromDateTxt = '';
        this.toDateTxt = '';
    }

    onDateSelection(date: NgbDate) {
        if (!this.fromDate && !this.toDate) {
          this.fromDate = date;
          this.fromDateTxt = format(this.FrmServ.NgDateToUTCString(this.fromDate), 'ddd DD MMM YYYY');
        } else if (this.fromDate && !this.toDate && date.after(this.fromDate)) {
          this.toDate = date;
          this.toDateTxt = format(this.FrmServ.NgDateToUTCString(this.toDate), 'ddd DD MMM YYYY');
          this.showDateRangePicker = false;
        } else {
          this.toDate = null;
          this.fromDate = date;
          this.fromDateTxt = format(this.FrmServ.NgDateToUTCString(this.fromDate), 'ddd DD MMM YYYY');
        }
      }

      isHovered(date: NgbDate) {
        return this.fromDate && !this.toDate && this.hoveredDate && date.after(this.fromDate) && date.before(this.hoveredDate);
      }

      isInside(date: NgbDate) {
        return date.after(this.fromDate) && date.before(this.toDate);
      }

      isRange(date: NgbDate) {
        return date.equals(this.fromDate) || date.equals(this.toDate) || this.isInside(date) || this.isHovered(date);
      }

    // search date: end

    // search and query: end

    editRowInModal(row, rowno) {
        this.ActiveDataRow = rowno;
        const modalRef = this.modalService.open(CrudFormComponent, { size: 'lg' });
        modalRef.componentInstance.UserId = row.UserId;
        modalRef.componentInstance.CrudId = row.CrudId;
        modalRef.result.then((result) => {
            console.log(result);
        }).catch((error) => {
            console.log(error);
        });
    }

    async saveForm () {
        console.log('saveForm');
        if (this.ActiveDataRow < 0) {
            this.data.changeMessage({ call: 'alert', errHead: 'Error', errMess: 'You have not loaded any data from the table above' });
            return;
        }
        // Update the table row
        this.DataList[this.ActiveDataRow] = this.SearchForm.value;
        // Save back to database
        // clean your data
        const newData = JSON.parse(JSON.stringify(this.SearchForm.value));
        // convert the date back into a string in UTC format
        const nd = this.FrmServ.NgDateToUTCString(newData.DateExampleBS);
        newData.DateExampleBS = nd;
        const Result = await this.FrmServ.SaveData('put', 'CrudExample', newData);
    }

    deleteRow(row, rowno) {
        this.data.changeMessage({
            call: 'alert',
            errHead: 'Alert',
            errMess: 'Are you sure you want to delete this row?',
            params: {
                buttontext: 'Delete',
                callfunction: 'dodelete',
                callback: { row, rowno },
            }
        });
    }
    async doDeleteRow(packet) {
        const delData = {
            Key: {
                UserId: { S: packet.row.UserId },
                CrudId: { S: packet.row.CrudId },
            }};
            const Result = await this.FrmServ.DeleteData('CrudExample', delData, false);
        this.DataList.splice(packet.rowno, 1);
    }

    addNewRecord(row, rowno) {
        this.ActiveDataRow = rowno;
        const modalRef = this.modalService.open(CrudFormComponent, { size: 'lg' });
        modalRef.componentInstance.UserId = 'new';
        modalRef.componentInstance.CrudId = hat();
        modalRef.result.then((result) => {
            console.log(result);
        }).catch((error) => {
            console.log(error);
        });
    }
}
