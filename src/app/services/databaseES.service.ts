import { Injectable, OnInit } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Utils } from './utils';
import { AuthService } from '../services/auth.service';
import { DataService } from './messenger.service';

@Injectable()
export class DBES implements OnInit {
    APIurl = 'https://c8ogcwi2t8.execute-api.eu-west-1.amazonaws.com/v1';
    message: any;
    dataModel: object;
    constructor(
        private data: DataService,
        public authService: AuthService,
        private http: HttpClient,
        private utils: Utils,
    ) {
    }

    ngOnInit() {
        this.data.currentMessage.subscribe(message => this.message = message);
    }

    async getModel(model) {
        const actUser = await this.authService.getAuthUserInfo();
        return new Promise((resolve, reject) => {
            const uj = actUser['jwt'];
            const headers = new HttpHeaders().set('Authorization', uj);
            this.http.post(this.APIurl + '/getmodel', { Model: model }, {headers})
            .subscribe(dt => {
                if (dt['errorMessage']) {
                    this.data.changeMessage({ call: 'LoaderHide' });
                    this.data.changeMessage({ call: 'alert', errHead: 'Error', errMess: dt['errorMessage'] });
                    resolve({ Count: 0, Items: [] });
                } else {
                    this.data.changeMessage({ call: 'LoaderHide' });
                    resolve(dt);
                }
            });
        });
    }

    async CreateES(index, type, model, body) {
        const actUser = await this.authService.getAuthUserInfo();
        return new Promise((resolve, reject) => {
            const ConvertData = '' + JSON.stringify(body);
            const Data = this.escapeSpecialChars(ConvertData);
            const newData = {
                Index: index,
                Type: type,
                Model: model,
                Body: Data
            };
            const uj = actUser['jwt'];
            const headers = new HttpHeaders().set('Authorization', uj);
            this.http.post(this.APIurl + '/create', newData, {headers})
            .subscribe(dt => {
                if (dt['errorMessage']) {
                    this.data.changeMessage({ call: 'LoaderHide' });
                    this.data.changeMessage({ call: 'alert', errHead: 'Error', errMess: dt['errorMessage'] });
                    resolve({ Count: 0, Items: [] });
                } else {
                    this.data.changeMessage({ call: 'LoaderHide' });
                    resolve(dt);
                }
            });
        });
    }

    async GetES(index, type, id, params) {
        const actUser = await this.authService.getAuthUserInfo();
        return new Promise((resolve, reject) => {
            const newData = {
                Index: index,
                Type: type,
                Params: params,
                Id: id
            };
            const uj = actUser['jwt'];
            const headers = new HttpHeaders().set('Authorization', uj);
            this.http.post(this.APIurl + '/get', newData, {headers})
            .subscribe(dt => {
                if (dt['errorMessage']) {
                    this.data.changeMessage({ call: 'LoaderHide' });
                    this.data.changeMessage({ call: 'alert', errHead: 'Error', errMess: dt['errorMessage'] });
                    resolve({ Count: 0, Items: [] });
                } else {
                    this.data.changeMessage({ call: 'LoaderHide' });
                    resolve(dt);
                }
            });
        });
    }

    async SearchES(index, type, model, body) {
        const actUser = await this.authService.getAuthUserInfo();
        return new Promise((resolve, reject) => {
            const ConvertData = '' + JSON.stringify(body);
            const Data = this.escapeSpecialChars(ConvertData);
            const newData = {
                Index: index,
                Type: type,
                Model: model,
                Body: Data
            };
            const uj = actUser['jwt'];
            const headers = new HttpHeaders().set('Authorization', uj);
            this.http.post(this.APIurl + '/search', newData, {headers})
            .subscribe(dt => {
                if (dt['errorMessage']) {
                    this.data.changeMessage({ call: 'LoaderHide' });
                    this.data.changeMessage({ call: 'alert', errHead: 'Error', errMess: dt['errorMessage'] });
                    resolve({ Count: 0, Items: [] });
                } else {
                    this.data.changeMessage({ call: 'LoaderHide' });
                    resolve(dt);
                }
            });
        });
    }
    async UpdateES(index, type, id, model, body) {
        const actUser = await this.authService.getAuthUserInfo();
        return new Promise((resolve, reject) => {
            const ConvertData = '' + JSON.stringify(body);
            const Data = this.escapeSpecialChars(ConvertData);
            const newData = {
                Index: index,
                Type: type,
                Id: id,
                Model: model,
                Body: Data
            };
            const uj = actUser['jwt'];
            const headers = new HttpHeaders().set('Authorization', uj);
            this.http.post(this.APIurl + '/update', newData, {headers})
            .subscribe(dt => {
                if (dt['errorMessage']) {
                    this.data.changeMessage({ call: 'LoaderHide' });
                    this.data.changeMessage({ call: 'alert', errHead: 'Error', errMess: dt['errorMessage'] });
                    resolve({ Count: 0, Items: [] });
                } else {
                    this.data.changeMessage({ call: 'LoaderHide' });
                    resolve(dt);
                }
            });
        });
    }
    async DeleteES(index, type, id) {
        const actUser = await this.authService.getAuthUserInfo();
        return new Promise((resolve, reject) => {
            const newData = {
                Index: index,
                Type: type,
                Id: id
            };
            const uj = actUser['jwt'];
            const headers = new HttpHeaders().set('Authorization', uj);
            this.http.post(this.APIurl + '/delete', newData, {headers})
            .subscribe(dt => {
                if (dt['errorMessage']) {
                    this.data.changeMessage({ call: 'LoaderHide' });
                    this.data.changeMessage({ call: 'alert', errHead: 'Error', errMess: dt['errorMessage'] });
                    resolve({ deleted: false });
                } else {
                    this.data.changeMessage({ call: 'LoaderHide' });
                    resolve({ deleted: true });
                }
            });
        });
    }
    async SendInviteES(LocationId, LocationName, Email, ToId, ToName, Subject, Body, Code) {
        const actUser = await this.authService.getAuthUserInfo();
        return new Promise((resolve, reject) => {
            // const ConvertData = '' + JSON.stringify(Body);
            Body = `${Body} \n\nYour invite code: ${Code} \n\n`;
            const newBody = this.escapeSpecialChars(Body);
            const newData = {
                LocationId,
                LocationName,
                Email,
                ToId,
                ToName,
                Subject,
                Body: newBody,
                Code
            };
            console.log('invite', newData);
            const uj = actUser['jwt'];
            const headers = new HttpHeaders().set('Authorization', uj);
            this.http.post(this.APIurl + '/sendinvite', newData, {headers})
            .subscribe(dt => {
                if (dt['errorMessage']) {
                    this.data.changeMessage({ call: 'LoaderHide' });
                    this.data.changeMessage({ call: 'alert', errHead: 'Error', errMess: dt['errorMessage'] });
                    resolve({ Count: 0, Items: [] });
                } else {
                    this.data.changeMessage({ call: 'LoaderHide' });
                    resolve(dt);
                }
            });
        });
    }
    async AcceptInviteEmailES( AccEmail, AccCode ) {
        const actUser = await this.authService.getAuthUserInfo();
        return new Promise((resolve, reject) => {
            const invObject = {
                Id: 'none',
                Email: AccEmail,
                Code: AccCode,
                Invitetype: 'email',
            };
            const uj = actUser['jwt'];
            const headers = new HttpHeaders().set('Authorization', uj);
            this.http.post(this.APIurl + '/acceptinvite', invObject, {headers})
            .subscribe(dt => {
                if (dt['errorMessage']) {
                    this.data.changeMessage({ call: 'LoaderHide' });
                    this.data.changeMessage({ call: 'alert', errHead: 'Error', errMess: dt['errorMessage'] });
                    resolve({ Count: 0, Items: [] });
                } else {
                    this.data.changeMessage({ call: 'LoaderHide' });
                    resolve(dt);
                }
            });
        });
    }
    async GetSelf(obj) {
        const actUser = await this.authService.getAuthUserInfo();
        return new Promise((resolve, reject) => {
            const uj = actUser['jwt'];
            const headers = new HttpHeaders().set('Authorization', uj);
            this.http.get(this.APIurl + '/getself', {headers})
            .subscribe(dt => {
                if (dt['errorMessage']) {
                    this.data.changeMessage({ call: 'LoaderHide' });
                    this.data.changeMessage({ call: 'alert', errHead: 'Error', errMess: dt['errorMessage'] });
                    resolve({ Count: 0, Items: [] });
                } else {
                    this.data.changeMessage({ call: 'LoaderHide' });
                    resolve(dt);
                }
            });
        });
    }
    escapeSpecialChars(text) {
        return text.replace(/[\\]/g, '\\\\')
        .replace(/[\"]/g, '\\\"')
        .replace(/[\/]/g, '\\/')
        .replace(/[\b]/g, '\\b')
        .replace(/[\f]/g, '\\f')
        .replace(/[\n]/g, '\\n')
        .replace(/[\r]/g, '\\r')
        .replace(/[\t]/g, '\\t');
    }
}

