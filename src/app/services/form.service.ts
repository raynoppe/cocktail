import { Injectable, OnInit } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { NgbDateParserFormatter, NgbDateStruct } from '@ng-bootstrap/ng-bootstrap';
import { FormBuilder, FormArray, FormGroup, FormControl } from '@angular/forms';
import { Utils } from './utils';
import { AuthService } from '../services/auth.service';

import { DataService } from './messenger.service';

@Injectable()
export class FormService implements OnInit {
    APIurl = 'https://842qwlm117.execute-api.eu-west-1.amazonaws.com/ve1';
    message: any;
    dataModel: object;
    dataModelForm: object;
    constructor(
        private data: DataService,
        public authService: AuthService,
        public formBuilder: FormBuilder,
        private http: HttpClient,
        private utils: Utils,
    ) {
    }

    ngOnInit() {
        this.data.currentMessage.subscribe(message => this.message = message);
    }

    escapeSpecialChars(text) {
        return text.replace(/[\\]/g, '\\\\')
        .replace(/[\"]/g, '\\\"')
        .replace(/[\/]/g, '\\/')
        .replace(/[\b]/g, '\\b')
        .replace(/[\f]/g, '\\f')
        .replace(/[\n]/g, '\\n')
        .replace(/[\r]/g, '\\r')
        .replace(/[\t]/g, '\\t');
    }

    convertModel(ModelIn, DataIn) {
        let matchData = false;
        if (DataIn !== null) {
            matchData = true;
        }
        this.dataModel = {};
        this.dataModelForm = {};
        for (const row of ModelIn) {
            let defaultData = row.default;
            if (matchData === true) {
                if (DataIn[row.name]) {
                    defaultData = DataIn[row.name];
                }
            }
            switch (row.type) {
                case 'number':
                this.dataModel[row.name] = defaultData * 1;
                this.dataModelForm[row.name] = new FormControl(defaultData * 1);
                break;
                case 'ignore':
                this.dataModel[row.name] = defaultData;
                this.dataModelForm[row.name] = [defaultData];
                break;
                case 'object':
                    this.dataModel[row.name] = defaultData;
                    const newObj = {};
                    const objData = Object.entries(defaultData);
                    for (const [Name, Val] of objData) {
                        newObj[Name] = [Val];
                    }
                    this.dataModelForm[row.name] = this.formBuilder.group(newObj);
                break;
                case 'array':
                    this.dataModel[row.name] = defaultData;
                    const newArr = [];
                    for (const rw of defaultData) {
                        const newArrRow = this.formBuilder.group(rw);
                        newArr.push(newArrRow);
                    }
                    this.dataModelForm[row.name] = this.formBuilder.array(newArr);
                break;
                case 'boolean':
                this.dataModel[row.name] = defaultData;
                this.dataModelForm[row.name] = new FormControl(defaultData);
                break;
                case 'date':
                let senddate = defaultData;
                if (typeof defaultData === 'string') {
                    senddate = this.DateToNgDate(new Date(defaultData));
                }
                this.dataModel[row.name] = senddate;
                this.dataModelForm[row.name] = new FormControl(senddate);
                break;
                default:
                this.dataModel[row.name] = '' + defaultData.trim();
                // this.dataModelForm[row.name] = ['' + row.default];
                const StrOut = '' + defaultData.trim();
                this.dataModelForm[row.name] = new FormControl(StrOut);
                break;
            }
        }
        return { dm: this.dataModel, dmf: this.dataModelForm };
    }

    DateToNgDate(date: Date): NgbDateStruct {
        return date ? {
            year: date.getUTCFullYear(),
            month: date.getUTCMonth() + 1,
            day: date.getUTCDate()
        } : null;
    }
    NgDateToDate(date: NgbDateStruct): Date {
        return date ? new Date(Date.UTC(date.year, date.month - 1, date.day, 0, 0, 0)) : null;
    }
    NgDateToUTCString(date: NgbDateStruct): string {
    return date ?
        `${date.year}-${this.utils.isNumber(date.month) ? this.utils.padNumber(date.month, 2, false) : ''}-${this.utils.isNumber(date.day) ? this.utils.padNumber(date.day, 2, false) : ''}` :
    '';
    }

    async dbSearch(FuncType, TableName, Query) {
        this.data.changeMessage({ call: 'Loader' });
        const actUser = await this.authService.getAuthUserInfo();
        return new Promise((resolve, reject) => {
            const uj = actUser['jwt'];
            const headers = new HttpHeaders().set('Authorization', uj);
            // create an array of search conditions
            // Both query and scan [{ operator: 'none', field: 'UserId', expression: '=', value: 'user1' }]
            const searchConditions = Query;
            const inQ = '' + JSON.stringify(searchConditions);
            const outQ = this.escapeSpecialChars(inQ);

            const qData = {
                // userId: "$context.authorizer.claims.sub", // this can be added automatically by the API gateway or you can send it. controlled on the API gateway
                FuncType: FuncType, // the name of the query you want to query
                TableName: TableName, // the name of the query you want to query
                Data: outQ,
            };
            this.http.post(this.APIurl + '/crud', qData, {headers})
            .subscribe(dt => {
                console.log('Message from API', dt);
                if (dt['errorMessage']) {
                    console.log('errorMessage', dt['errorMessage']);
                    this.data.changeMessage({ call: 'LoaderHide' });
                    this.data.changeMessage({ call: 'alert', errHead: 'Error', errMess: dt['errorMessage'] });
                    resolve({ Count: 0, Items: [] });
                } else {
                    this.data.changeMessage({ call: 'LoaderHide' });
                    resolve(dt);
                }
            });
        });
    }

    async MatchModel (Data, Model, FormatRules) {
        const newRow = await this.MatchCreateRow(Model);
        return new Promise((resolve, reject) => {
            const NewList = [];
            for (const rw of Data) {
                const rowNewRow = JSON.parse(JSON.stringify(newRow));
                const objData = Object.entries(rw);
                for (let index = 0; index < objData.length; index++) {
                    const element = objData[index];
                    const Name = JSON.parse(JSON.stringify(element[0]));
                    const Val = JSON.parse(JSON.stringify(element[1]));
                    rowNewRow[Name] = Val;
                }
                // apply formating
                const nrwData = Object.entries(rowNewRow);
                for (let index = 0; index < nrwData.length; index++) {
                    const element = nrwData[index];
                    const Name = JSON.parse(JSON.stringify(element[0]));
                    let Val = JSON.parse(JSON.stringify(element[1]));
                    for (const frw of FormatRules) {
                        if (frw['field'] === Name) {
                            switch (frw['format']) {
                                case 'number':
                                Val = parseInt(Val, 10);
                                break;
                                case 'date':
                                Val = this.DateToNgDate(new Date(Val));
                                break;
                                default:

                                break;
                            }
                        }
                    }
                    rowNewRow[Name] = Val;
                }
                NewList.push(rowNewRow);
            }
            resolve(NewList);
        });
    }

    MatchCreateRow (Model) {
        return new Promise((resolve, reject) => {
            const newRow = {};
            for (const mrw of Model) {
                switch (mrw.type) {
                    case 'number':
                        newRow[mrw.name] = mrw.default * 1;
                    break;
                    case 'object':
                        newRow[mrw.name] = mrw.default;
                    break;
                    case 'array':
                        newRow[mrw.name] = mrw.default;
                    break;
                    case 'boolean':
                        newRow[mrw.name] = mrw.default;
                    break;
                    case 'date':
                        const senddate = this.DateToNgDate(new Date(mrw.default));
                        newRow[mrw.name] = mrw.default;
                    break;
                    default:
                        newRow[mrw.name] = '' + mrw.default;
                    break;
                }
            }

            resolve(newRow);
        });
    }

    FixDataForSave(inData) {
        const objData = Object.entries(inData);
        for (const [Name, Val] of objData) {
            if ( typeof Val === 'string' ) {
                if (Val === '') {
                    inData[Name] = Val + ' ';
                }
            }
            if ( typeof Val === 'object' ) {
                if (Val instanceof Array) {
                    const newArray = [];
                    for (let index = 0; index < Val.length; index++) {
                        const oldRow = Val[index];
                        const newRow = this.FixDataForSave(oldRow);
                        newArray.push(newRow);
                    }
                    inData[Name] = newArray;
                } else {
                    const newObj = this.FixDataForSave(Val);
                    inData[Name] = newObj;
                }
            }
        }
        return inData;
    }

    async SaveData(FuncType, TableName, inData) {
        // this.data.changeMessage({ call: 'Loader' });
        const actUser = await this.authService.getAuthUserInfo();
        return new Promise((resolve, reject) => {
            const uj = actUser['jwt'];
            const headers = new HttpHeaders().set('Authorization', uj);
            inData = this.FixDataForSave(inData);
            console.log('inData', inData);

            const ConvertData = '' + JSON.stringify(inData);
            const Data = this.escapeSpecialChars(ConvertData);
            const sData = {
                // userId: "$context.authorizer.claims.sub", // this can be added automatically by the API gateway or you can send it. controlled on the API gateway
                FuncType, // the name of the query you want to query
                TableName, // the name of the query you want to query
                Data,
            };
            this.http.post(this.APIurl + '/crud', sData, {headers})
            .subscribe(dt => {
                console.log('Result from API', dt);
                if (dt['errorMessage']) {
                    console.log('errorMessage', dt['errorMessage']);
                    this.data.changeMessage({ call: 'LoaderHide' });
                    this.data.changeMessage({ call: 'alert', errHead: 'Error', errMess: dt['errorMessage'] });
                    resolve({ Count: 0, Items: [] });
                } else {
                    this.data.changeMessage({ call: 'LoaderHide' });
                    this.data.changeMessage({ call: 'alert', errHead: 'Alert', errMess: 'Your data has been saved' });
                    resolve(dt);
                }
            });
        });
    }
    async DeleteData(TableName, inData, showconfirm = true) {
        this.data.changeMessage({ call: 'Loader' });
        const actUser = await this.authService.getAuthUserInfo();
        return new Promise((resolve, reject) => {
            const uj = actUser['jwt'];
            const headers = new HttpHeaders().set('Authorization', uj);
            const ConvertData = '' + JSON.stringify(inData);
            const Data = this.escapeSpecialChars(ConvertData);
            const sData = {
                // userId: "$context.authorizer.claims.sub", // this can be added automatically by the API gateway or you can send it. controlled on the API gateway
                FuncType: 'delete', // the name of the query you want to query
                TableName, // the name of the query you want to query
                Data,
            };
            this.http.post(this.APIurl + '/crud', sData, {headers})
            .subscribe(dt => {
                console.log('Result from API (Delete)', dt);
                if (dt['errorMessage']) {
                    console.log('errorMessage', dt['errorMessage']);
                    this.data.changeMessage({ call: 'LoaderHide' });
                    this.data.changeMessage({ call: 'alert', errHead: 'Error', errMess: dt['errorMessage'] });
                    resolve({ Count: 0, Items: [] });
                } else {
                    this.data.changeMessage({ call: 'LoaderHide' });
                    if (showconfirm) {
                        this.data.changeMessage({ call: 'alert', errHead: 'Alert', errMess: 'Your data has been deleted' });
                    }
                    resolve(dt);
                }
            });
        });
    }

    async GetCountries() {
        return new Promise((resolve, reject) => {
            this.http.get('https://restcountries.eu/rest/v2/all?fields=name;alpha2Code', {responseType: 'json'})
            .subscribe(CountryList => {
                resolve(CountryList);
            });
        });
    }
}
