export function loremModel() {
    const loremData = {
        male: [
            { text: 'John Smith', sex: 'male', dob: '1984-01-18', image: 'http://i.pravatar.cc/200?img=59' },
            { text: 'Charlie Parker', sex: 'male', dob: '1965-08-04', image: 'http://i.pravatar.cc/200?img=69' },
            { text: 'Rickus Von Dicus', sex: 'male', dob: '1981-10-01', image: 'http://i.pravatar.cc/200?img=56' },
            { text: 'Shuan Miller', sex: 'male', dob: '1979-07-12', image: 'http://i.pravatar.cc/200?img=14' },
            { text: 'Lee Chan', sex: 'male', dob: '1985-03-27', image: 'http://i.pravatar.cc/200?img=33' },
        ],
        female: [
            { text: 'Jane Smith', sex: 'female', dob: '1984-01-18', image: 'http://i.pravatar.cc/200?img=47' },
            { text: 'Chantelle Lee', sex: 'female', dob: '1965-08-04', image: 'http://i.pravatar.cc/200?img=31' },
            { text: 'Suzy Jones', sex: 'female', dob: '1981-10-01', image: 'http://i.pravatar.cc/200?img=28' },
            { text: 'Betty Miller', sex: 'female', dob: '1979-07-12', image: 'http://i.pravatar.cc/200?img=26' },
            { text: 'Claudette Van Derbiljt', sex: 'female', dob: '1989-02-14', image: 'http://i.pravatar.cc/200?img=16' },
        ],
        word: [
            { text: 'Sonder' },
            { text: 'Eloquence' },
            { text: 'Serendipity' },
            { text: 'Redamancy' },
            { text: 'Eunoia' },
            { text: 'Meraki' },
            { text: 'Goya' },
            { text: 'Felicity' },
            { text: 'Resplendence' },
            { text: 'Oblivion' },
        ],
        short: [
            { text: 'Lorem ipsum in lectus senectus.' },
            { text: 'Praesent lacus duis molestie.' },
            { text: 'Commodo suspendisse ullamcorper.' },
            { text: 'Donec at lectus sit.' },
            { text: 'In justo erat, varius.' },
        ],
        medium: [
            { text: 'Suspendisse potenti. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Morbi et ante sed dolor.' },
            { text: 'Tempor vitae faucibus mollis curae semper torquent platea fusce donec, curabitur curae purus molestie lacus nostra senectus convallis congue, fermentum libero quis nostra nullam sodales vel nisl.' },
            { text: 'Praesent lacus duis molestie in primis fermentum, sem sed litora lobortis dictumst curabitur sem, interdum auctor fermentum mollis tempus.' },
            { text: 'Quisque a dignissim tellus, a dictum lorem. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae;.' },
            { text: 'Sed id lobortis turpis. Duis condimentum, nulla at rhoncus aliquet, mi leo lacinia odio, ac viverra ligula magna vehicula tellus.' },
        ],
        long: [
            { text: 'Sagittis ornare per purus vivamus conubia in tellus ullamcorper, donec sollicitudin proin venenatis primis sapien ante feugiat, lobortis aliquam ligula malesuada nisi commodo faucibus cubilia litora himenaeos pellentesque nullam bibendum aenean nisl malesuada semper, vel accumsan adipiscing mauris ad netus ac duis pulvinar, consequat elementum id nam inceptos malesuada felis nisl dictum accumsan adipiscing donec sapien taciti ut tortor magna non taciti vestibulum.' },
            { text: 'A vehicula proin tempus donec lorem arcu lobortis, at lobortis vel arcu urna vivamus quis porta, vehicula class vehicula torquent condimentum massa etiam magna consequat pharetra ut elementum magna enim curabitur condimentum tellus malesuada hendrerit ante felis fermentum, sed eget malesuada facilisis vehicula, sed cursus laoreet habitasse netus quisque dui arcu etiam conubia inceptos ante, donec ac tortor sollicitudin augue lorem torquent, in iaculis eleifend ad velit proin tincidunt iaculis quisque dolor senectus rhoncus posuere, metus tempor aenean venenatis id vel, platea tincidunt per habitasse euismod.' },
            { text: 'Purus fringilla fermentum lorem nisl tempor est ac sagittis elementum, arcu taciti justo ad suspendisse in litora class aenean habitasse, blandit turpis aliquam convallis viverra interdum augue commodo potenti id tristique mollis orci gravida fringilla etiam gravida id, dolor semper sociosqu interdum sem porta ornare habitant feugiat, tempor ultricies rhoncus ac commodo dictum feugiat quisque placerat bibendum curae nullam vulputate vehicula ad litora sociosqu litora integer, curabitur lorem sed mollis aliquet sagittis convallis lorem eget sem phasellus etiam eleifend rhoncus eros a litora in, placerat urna imperdiet nullam in morbi tempus nec, lacus vulputate cras enim cras velit porttitor.' },
            { text: 'Nam efficitur ante ipsum, non finibus sapien ullamcorper sed. Nunc consequat massa quis tortor condimentum eleifend. Morbi eget dolor sollicitudin lorem tempor imperdiet et eget eros. Quisque ultrices bibendum justo, ac vestibulum lorem condimentum quis. Duis sapien nunc, sodales sed aliquet ac, commodo vel urna. Cras erat erat, bibendum nec odio at, vestibulum finibus odio. Cras molestie consequat dapibus. Mauris dui orci, vehicula a leo condimentum, maximus congue dui. Nulla rhoncus tristique hendrerit. Cras commodo a turpis eget tristique.' },
            { text: 'Morbi lacinia diam a tellus facilisis imperdiet. Proin fringilla pharetra sem, tempus maximus quam faucibus sollicitudin. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Cras ultrices suscipit eros. Aenean tempus tempus eros ut hendrerit. Sed maximus est neque, ac tristique eros interdum quis. Phasellus efficitur mollis libero, id pretium augue iaculis ac. Pellentesque vehicula mauris rutrum suscipit lobortis. Maecenas faucibus mattis efficitur. Morbi rhoncus ac lorem at condimentum. Nullam ut sapien nec dui varius finibus ut et lacus. Proin at hendrerit massa. Integer quis tortor sed lorem imperdiet sollicitudin. Ut ac metus sagittis, ornare massa.' },
        ]
    };
    return loremData;
  }
