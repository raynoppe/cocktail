import { Component, OnInit, ViewChild, Injectable } from '@angular/core';
import { routerTransition } from '../../router.animations';
import { FormBuilder, FormArray, FormGroup } from '@angular/forms';
import { NgbActiveModal, NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { AuthService } from '../../services/auth.service';
import { crudModel } from '../../models/crud.model';
import { loremModel } from '../../models/lorem.model';
import { FormService } from '../../services/form.service';
import { DataService } from '../../services/messenger.service';
import { CrudFormComponent } from '../crud-form/crud-form.component';
import * as hat from 'hat';
// import { FormModalComponent } from '../../form-modal/form-modal.component';

@Component({
    selector: 'app-crud-page',
    templateUrl: './crud-page.component.html',
    styleUrls: ['./crud-page.component.scss'],
    animations: [routerTransition()],
})
export class CrudPageComponent implements OnInit {
    message: any;
    CrudForm: FormGroup;
    CrudData: any;
    CrudDataForm: any;
    dti: any;
    isLoading = false;
    saveError = false;
    DataList: any;
    DataCount: any;
    DataListRaw: any;
    DataCountRaw: any;
    QueryString: any;
    ShowMore: boolean;
    ActiveDataRow: any;
    // used for random records
    LastColor = 'red';
    LastGender = 'male';
    LastMale = 0;
    LastFemale = 0;
    LastCheckBox1 = true;
    LastCheckBox2 = false;
    LastWord = 0;
    LastShort = 0;
    LastMedium = 0;
    LastLong = 0;

    constructor(
        public formBuilder: FormBuilder,
        public FrmServ: FormService,
        public authService: AuthService,
        private data: DataService,
        private modalService: NgbModal,
        ) {
        this.dti = FrmServ.convertModel(crudModel(), null); // load the AWS user model

        this.CrudData = this.dti.dm; // set the form model

        this.DataList = [];
        this.DataCount = 0;
        this.DataListRaw = [];
        this.DataCountRaw = 0;
        this.ActiveDataRow = -1;
        this.QueryString = {
            ExpressionAttributeValues: { ':UI': { S: 'user1' } },
            KeyConditionExpression: 'UserId = :UI'
        };
        this.ShowMore = false;
    }

    ngOnInit() {
        // this.data.currentMessage.subscribe(message => this.message = message);
        this.data.currentMessage.subscribe((message) => {
            this.message = message;
            if (this.message.call === 'dodelete') {
                this.doDeleteRow(this.message.data);
            }
        });
        this.CrudForm = this.formBuilder.group(this.dti.dmf);
        this.LoadData();
    }

    async LoadData() {
        this.data.changeMessage({ call: 'Loader' });
        const actUser = await this.authService.getAuthUserInfo();
        this.QueryString.ExpressionAttributeValues = { ':UI': { S: actUser['sub'] } },
        this.QueryString.KeyConditionExpression = 'UserId = :UI';
        /*
        Call: dbSearch(FuncType, TableName, Params, Conditions)
        In File: /src/app/services/form.services.ts
        Description: Get data from the database useing a query
        Attributes:
        FuncType: query or scan
        TableName: The table name in DynamoDB
        Query:
            ExpressionAttributeValues: { ':UI': { S: 'user1' } } *Required
            KeyConditionExpression: 'UserId = :UI' *Required Operators: = | < | <= | > | >= | BETWEEN | begins_with ( sortKeyName, :sortkeyval )
            IndexName: If you are using an index against the table UserId-DateExampleBS-index
            Limit: 10 *Integer
            ProjectionExpression: 'UserId, StringExample' String with a list of fields you want returned
            ExpressionAttributeNames: {"#U":"UserId"}
            ExclusiveStartKey
            For more information: https://docs.aws.amazon.com/AWSJavaScriptSDK/latest/AWS/DynamoDB.html#query-property
        */
        const getQuery = await this.FrmServ.dbSearch(
            'query',
            'CrudExample',
            this.QueryString
        );
        this.DataListRaw = getQuery['Items'];
        this.DataCountRaw = getQuery['Count'];
        // pagination.
        if (getQuery['LastEvaluatedKey']) {
            this.QueryString['ExclusiveStartKey'] = getQuery['LastEvaluatedKey'];
            this.ShowMore = true;
        }
        const FormatRules = [
            { 'field': 'NumberExample', 'format': 'number' },
            { 'field': 'DateExampleBS', 'format': 'date' },
            { 'field': 'ImageExample', 'format': 'string' },
        ];
        this.DataList = await this.FrmServ.MatchModel(this.DataListRaw, crudModel(), FormatRules);
        this.data.changeMessage({ call: 'LoaderHide' });
    }

    sortSearch() {
        this.QueryString['IndexName'] = 'UserId-DateExampleBS-index';
        this.LoadData();
    }

    limitSearch() {
        console.log('Limit search');
        this.QueryString['Limit'] = 2;
        this.LoadData();
    }

    loadMore() {
        console.log('Load More');
        this.QueryString['ScanIndexForward'] = true;
        this.LoadData();
    }

    loadPrevious() {
        this.QueryString['ScanIndexForward'] = false;
        console.log('Load Previous');
        this.LoadData();
    }
    dataReset() {
        console.log('Reset');
        this.ShowMore = false;
        this.QueryString = {
            ExpressionAttributeValues: { ':UI': { S: 'user1' } },
            KeyConditionExpression: 'UserId = :UI'
        };
        this.LoadData();
    }

    editRow(row, rowno) {
        /*
        if you don't have any arrays in your data you can update the original data with patcValue:
        this.CrudData = row;
        this.CrudForm.patchValue(row);
        */
       this.ActiveDataRow = rowno;
        const rowFormData = this.FrmServ.convertModel(crudModel(), row); // load the AWS user model
        console.log('rowFormData', rowFormData);
        this.CrudData = rowFormData.dm; // set the form model
        this.CrudForm = this.formBuilder.group(rowFormData.dmf);
    }

    editRowInModal(row, rowno) {
        this.ActiveDataRow = rowno;
        const modalRef = this.modalService.open(CrudFormComponent, { size: 'lg' });
        modalRef.componentInstance.UserId = row.UserId;
        modalRef.componentInstance.CrudId = row.CrudId;
        modalRef.result.then((result) => {
            console.log(result);
        }).catch((error) => {
            console.log(error);
        });
    }

    async saveForm () {
        console.log('saveForm');
        if (this.ActiveDataRow < 0) {
            this.data.changeMessage({ call: 'alert', errHead: 'Error', errMess: 'You have not loaded any data from the table above' });
            return;
        }
        // Update the table row
        this.DataList[this.ActiveDataRow] = this.CrudForm.value;
        // Save back to database
        // clean your data
        const newData = JSON.parse(JSON.stringify(this.CrudForm.value));
        // convert the date back into a string in UTC format
        const nd = this.FrmServ.NgDateToUTCString(newData.DateExampleBS);
        newData.DateExampleBS = nd;
        const Result = await this.FrmServ.SaveData('put', 'CrudExample', newData);
    }

    deleteRow(row, rowno) {
        this.data.changeMessage({
            call: 'alert',
            errHead: 'Alert',
            errMess: 'Are you sure you want to delete this row?',
            params: {
                buttontext: 'Delete',
                callfunction: 'dodelete',
                callback: { row, rowno },
            }
        });
    }
    async doDeleteRow(packet) {
        const delData = {
            Key: {
                UserId: { S: packet.row.UserId },
                CrudId: { S: packet.row.CrudId },
            }};
            const Result = await this.FrmServ.DeleteData('CrudExample', delData);
        // this.DataList.splice(packet.rowno, 1);
    }

    // array functions
    createItem(): FormGroup {
        return this.formBuilder.group({
            RowField1: '',
            RowField2: ''
        });
      }

    AddANewRow() {
        const items = this.CrudForm.get('ArrayExample') as FormArray;
        items.push(this.createItem());
    }

    DeleteARow(rowno) {
        const items = this.CrudForm.get('ArrayExample') as FormArray;
        items.removeAt(rowno);
    }

    addNewRecord(row, rowno) {
        this.ActiveDataRow = rowno;
        const modalRef = this.modalService.open(CrudFormComponent, { size: 'lg' });
        modalRef.componentInstance.UserId = 'new';
        modalRef.componentInstance.CrudId = hat();
        modalRef.result.then((result) => {
            console.log(result);
        }).catch((error) => {
            console.log(error);
        });
    }

    async addRandomRecord() {
        /*
        LastColor = 'red';
        LastGender = 'male';
        LastMale = 0;
        LastFemale = 0;
        LastCheckBox1 = true;
        LastCheckBox2 = false;
        LastWord = 0;
        LastShort = 0;
        LastMedium = 0;
        LastLong = 0;
        */
        const actUser = await this.authService.getAuthUserInfo();
        const dmt = loremModel();
        const newData = {};
        newData['UserId'] = actUser['sub'];
        newData['CrudId'] = hat();
        // ArrayExample
        newData['ArrayExample'] = [];
        const newArrRow = {};
        newArrRow['RowField1'] = dmt.word[this.LastWord].text;
        if (this.LastWord === 9) { this.LastWord = 0; } else { this.LastWord += 1; }
        newArrRow['RowField2'] = dmt.word[this.LastWord].text;
        if (this.LastWord === 9) { this.LastWord = 0; } else { this.LastWord += 1; }
        newData['ArrayExample'].push(newArrRow);
        // ArrayExample: end
        newData['CheckBoxExample'] = this.LastCheckBox1;
        newData['CheckBoxExample2'] = this.LastCheckBox2;
        if (this.LastCheckBox1) { this.LastCheckBox1 = false; } else { this.LastCheckBox1 = true; }
        if (this.LastCheckBox2) { this.LastCheckBox2 = false; } else { this.LastCheckBox2 = true; }
        let useString = '';
        let useDate = '';
        let useImg = '';
        if (this.LastGender === 'male') {
            useString = dmt.male[this.LastMale].text;
            useDate = dmt.male[this.LastMale].dob;
            useImg = dmt.male[this.LastMale].image;
            newData['SelectExample'] = this.LastGender;
            this.LastGender = 'female';
        } else {
            useString = dmt.female[this.LastMale].text;
            useDate = dmt.female[this.LastMale].dob;
            useImg = dmt.female[this.LastMale].image;
            newData['SelectExample'] = this.LastGender;
            this.LastGender = 'male';
        }
        newData['StringExample'] = useString;
        newData['DateExample'] = useDate;
        newData['DateExampleBS'] = useDate;
        newData['ImageExample'] = useImg;
        newData['ImageAsBase64'] = '';
        newData['NumberExample'] = Math.floor(Math.random() * Math.floor(1000));

        const newObject = {};
        newObject['Field1'] = dmt.word[this.LastWord].text;
        if (this.LastWord === 9) { this.LastWord = 0; } else { this.LastWord += 1; }
        newObject['Field2'] = dmt.word[this.LastWord].text;
        if (this.LastWord === 9) { this.LastWord = 0; } else { this.LastWord += 1; }
        newData['ObjectExample'] = newObject;

        newData['RadioButton'] = this.LastColor;

        newData['TextAreaExample'] = dmt.medium[this.LastMedium].text;
        newData['TextAreaExampleLong'] = dmt.long[this.LastLong].text;

        console.log('newData', newData);
        if (this.LastColor === 'red') { this.LastColor = 'white'; } else { this.LastColor = 'red'; }
        if (this.LastMale === 4) { this.LastMale = 0; } else { this.LastMale += 1; }
        if (this.LastFemale === 4) { this.LastFemale = 0; } else { this.LastFemale += 1; }
        if (this.LastShort === 4) { this.LastShort = 0; } else { this.LastShort += 1; }
        if (this.LastMedium === 4) { this.LastMedium = 0; } else { this.LastMedium += 1; }
        if (this.LastLong === 4) { this.LastLong = 0; } else { this.LastLong += 1; }
        // Save back to database
        // clean your data
        const outData = JSON.parse(JSON.stringify(newData));
        // convert the date back into a string in UTC format
        const Result = await this.FrmServ.SaveData('put', 'CrudExample', newData);
        const senddate = this.FrmServ.DateToNgDate(new Date(newData['DateExampleBS']));
        newData['DateExampleBS'] = senddate;
        this.DataList.push(newData);
    }
}
