import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { AccountRoutingModule } from './account-routing.module';
import { AccountComponent } from './account.component';
import { PageHeaderModule } from '../../shared';
import { FormsModule, ReactiveFormsModule} from '@angular/forms';

@NgModule({
    imports: [
        CommonModule,
        AccountRoutingModule,
        NgbModule.forRoot(),
        PageHeaderModule,
        FormsModule,
        ReactiveFormsModule,
    ],
    declarations: [
        AccountComponent
    ]
})
export class AccountModule {}
