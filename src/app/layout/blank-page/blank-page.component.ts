import { Component, OnInit } from '@angular/core';
import { routerTransition } from '../../router.animations';
import { DataService } from '../../services/messenger.service';

@Component({
    selector: 'app-blank-page',
    templateUrl: './blank-page.component.html',
    styleUrls: ['./blank-page.component.scss'],
    animations: [routerTransition()]
})
export class BlankPageComponent implements OnInit {
    message: any;
    constructor(
        private data: DataService
    ) {
        // initial code here
    }

    ngOnInit() {
        this.data.currentMessage.subscribe(message => this.message = message);
        // this.data.changeMessage({ call: 'alert', errHead: 'Alert', errMess: 'Your content has been loaded' });
    }
}
