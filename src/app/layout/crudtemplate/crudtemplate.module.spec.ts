import { CrudTemplateModule } from './crudtemplate.module';

describe('CrudTemplateModule', () => {
    let crudTemplateModule: CrudTemplateModule;

    beforeEach(() => {
        crudTemplateModule = new CrudTemplateModule();
    });

    it('should create an instance', () => {
        expect(crudTemplateModule).toBeTruthy();
    });
});
